#!/bin/bash

# This approach produces garbace, because
# dir structure does NOT reflect any resource-errID mapping

git clone https://github.com/accurics/terrascan/
cd terrascan/pkg/policies/opa/rego/aws

for FILE in $(ls */*.json)
do
cat $FILE | jq -r '"\(.resource_type),\(.reference_id)"' >> terrascan.1.csv
done