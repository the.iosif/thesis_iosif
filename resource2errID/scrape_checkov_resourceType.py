#!/usr/bin/env python3
from   selenium.webdriver.support    import expected_conditions as EC
from   selenium.webdriver.support.ui import WebDriverWait
from   selenium.webdriver.common.by  import By
from   selenium                      import webdriver
from   bs4                           import BeautifulSoup
import json
import sys
import re



# global constants
DEBUG    = True 
BASE_URL = 'https://www.checkov.io/5.Policy%20Index/terraform.html'
DRIVER   = '/usr/lib/chromium-browser/chromedriver'

def reqPageSource(URL, DRIVER=DRIVER):
    """
    returns HTML page source from URL
    using DRIVER for Selenium
    """
    options = webdriver.ChromeOptions() 
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
    +"AppleWebKit/537.36 (KHTML, like Gecko)"
    +"Chrome/87.0.4280.141 Safari/537.36")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    driver = webdriver.Chrome(options=options, executable_path=DRIVER)
    driver.get(URL)
    htmlSrc = driver.page_source
    driver.quit()
    return htmlSrc


# check with https://tfsec.dev/docs/aws/home/ to see updated list
# (initial implementation below left for posterity)
# tf_aws_codes = [f"AWS{str(x+1).zfill(3)}" for x in range(88)]

if __name__ == '__main__':
# In[]
   # curl https://raw.githubusercontent.com/bridgecrewio/checkov/master/docs/5.Policy%20Index/terraform.md | tac | tac | grep CKV_AWS | tr -s " " | cut -d "|" -f 3,5 | sed "s/ | /,/g" | sed "s/\s//g" > ~/thesis_iosif/checkovResourcesErrID.csv
        if len(set(resource)) > 1:
            print(f"Manual check needed: {errID}: {resource}")
            input("...")
        else:
            resource = resource[0]
            if resource not in resourceDict.keys():
                resourceDict[resource] = [errID]
            else:
                resourceDict[resource].append(errID)
        _i += 1

    with open ('resource2errID.json', 'w+') as f:
        json.dump(resourceDict, f)
    if False:
        with open ('resource2errID_checkov.json', 'w+') as f:
            json.dump(resourceDict, f)

    #print("EXTRA: At the end I had to go to tfsec.dev/docs/general and add those manually.")