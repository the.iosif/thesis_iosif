#!/usr/bin/env python3
import pandas as pd
import json
import sys
import re


df = pd.read_csv('checkovPolicyIndex.md.csv')

# In[]
resourceDict = {}

for _, row in df.iterrows():
    resource, errID = row['Entity'], row['Id']

    if resource not in resourceDict.keys():
        resourceDict[resource] = [errID]
    else:
        resourceDict[resource].append(errID)

# In[]
with open ('resource2errID_checkov.json', 'w+') as f:
    json.dump(resourceDict, f)

#print("EXTRA: At the end I had to go to tfsec.dev/docs/general and add those manually.")
