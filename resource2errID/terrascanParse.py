#!/usr/bin/env python3
import json
import os

# Generating the 'terrascan.1.csv' file
if not os.path.exists('terrascan.1.csv'):
    os.system('./terrascanGenCsv.sh')

with open ('terrascan.1.csv', 'r')  as f:
    lines = f.readlines()
    lines = [l.strip() for l in lines]

resourceDict = {}

for line in lines:
    resource, errID = line.split(',')

    if resource not in resourceDict.keys():
        resourceDict[resource] = [errID]
    else:
        resourceDict[resource].append(errID)

with open ('resource2errID_terrascan.json', 'w+') as f:
    json.dump(resourceDict, f)