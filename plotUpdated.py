#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tabular approach
"""

# Imports & Definitions
from   datetime           import datetime
import matplotlib.patches as     mpatches
import matplotlib.pyplot  as     plt
import pandas             as     pd
import numpy              as     np
import subprocess
import json
import re
import os

# Hardcoding hacky one-and-done business
USER    = os.getenv('USER') #preserve multi-system compatibility
FIGDIR  = f'/home/{USER}/thesis_iosif/figures'
DATADIR = f'/home/{USER}/tf_sast_parsed'

TOOLS   = ['tfsec', 'terrascan', 'checkov']
SAVE    = False

def executeOS(cmd):
    x = subprocess.run([cmd], shell=True, capture_output=True, text=True)
    return x

def labelTheBars(barPlot, barLabels):
    '''
    Credit where credit is due:
    https://moonbooks.org/Articles/How-to-add-text-on-a-bar-with-matplotlib-/
    '''
    print(len(barPlot), len(barLabels))
    for idx,rect in enumerate(barPlot):
        #height = rect.get_height()
        plt.text(x=rect.get_x() + rect.get_width()/2., y=0.4,
                s=barLabels[idx],
                ha='center', va='baseline', rotation=90, color='black')

colorLookup = {'alb': 'darkgrey',
               'alb_listener': 'darkgrey',
               'cloudtrail': 'gainsboro',
               'cloudwatch': 'peachpuff',
               'cloudwatch_log_group': 'peachpuff',
               'config': 'grey',  ###
               'db_instance': 'darkorange',
               'db_security_group': 'darkorange',
               'elasticache_security_group': 'purple',
               'elb': 'black', ####
               'iam_role_policy': 'lightcoral',
               'instance': 'yellow',
               'lambda_function': 'yellowgreen',
               'launch_configuration': 'palegreen',
               'launch_template': 'darkseagreen',
               'lb_listener': 'teal',
               'lb_target_group': 'teal',
               'redshift_security_group': 'purple', ####
               's3_bucket': 'red',
               's3_bucket_object': 'red',
               'security_group': 'darkcyan',
               'security_group_rule': 'darkcyan',
               'subnet': 'dodgerblue',
               'vpc': 'blue',
               'provider': 'grey' ###
              }

# In[ res2errID creation]
# | Resource | ErrorID list | len(errIDlist) | tool|
res2errID = {}
for TOOL in TOOLS:
    with open(f'/home/{USER}/thesis_iosif/resource2errID/resource2errID_{TOOL}.json', 'r') as f:
        _res2errID_dict    = json.load(f)
        res2errID[f'{TOOL}'] = pd.Series(_res2errID_dict, name=f'{TOOL}')
res2errID = pd.concat([res2errID['tfsec'], res2errID['terrascan'], res2errID['checkov']], axis=1)


# NaN's from missing resources are replaces with an empty list [ ]
res2errID = res2errID.apply(lambda s: s.fillna({i: [] for i in res2errID.index}))
res2errID['allIDs'] = res2errID['tfsec'] + res2errID['terrascan'] + res2errID['checkov']

for TOOL in TOOLS:
    res2errID[f'N_{TOOL}'] = res2errID[f'{TOOL}'].str.len()

res2errID['N_avg']  = res2errID[[f'N_{T}' for T in TOOLS ]].mean(axis=1)
res2errID['N_var']  = res2errID[[f'N_{T}' for T in TOOLS ]].var(axis=1)
res2errID['N_sum']  = res2errID[[f'N_{T}' for T in TOOLS ]].sum(axis=1)
res2errID.reset_index(inplace=True)
res2errID.rename(columns={'index':'resource'}, inplace=True)


# In[ errID2res creation]
# | ErrorID | resource list |
# We'll need the reverse mapping later,
# And this is the hackiest yet fastest way to get there
errID2res = {}
for _, row in res2errID.iterrows():
    resource  = row.resource
    errIDlist = row.allIDs
    for errID in errIDlist:
        # errID-resource is one2many in a few cases, hence the list approach.
        if errID not in errID2res.keys():
            errID2res[errID] = [resource]
        else:
            errID2res[errID].append(resource)

errID2res_dict = errID2res
errID2res      = pd.DataFrame(columns=['ruleID', 'resource'])

for errID, resource in errID2res_dict.items():
    errID2res = errID2res.append({'ruleID'  : errID,
                                  'resource': resource},
                                 ignore_index=True)
# In[ resource occurence count creation ]
resourceOccurrence = pd.read_csv(f"{DATADIR}/resourceCount.csv")
resourceOccurrence['name']=resourceOccurrence['name'].str.strip() # some labels have trailing whitespace
resourceOccurrence['type']=resourceOccurrence['name'].str.split(' ').apply(lambda x: x[0])
resourceOccurrence['name']=resourceOccurrence['name'].str.split(' ').apply(lambda x: x[-1])

# keep only AWS / generic stuff
resourceOccurrence = resourceOccurrence[resourceOccurrence['name'].isin(['variable', 'output']) | resourceOccurrence['name'].str.startswith('aws')]

allSummed = sum(resourceOccurrence.occurences.values)
resourceOccurrence['percentage'] = resourceOccurrence.apply(lambda row: round(row.occurences/allSummed*100, 2), axis=1)

# In[ rules occurence creation ]
# OCCURENCES | RULEID   | TOOL   | RESOURCE
#  i.e.69    |  AWS420 |  TFSEC  | AWS_S3_BUCKET
ruleOccurrence = pd.DataFrame()

#TODO: what about multi-resource rules?

for TOOL in TOOLS:
    _ruleOccurrence_df = pd.read_csv(f"{DATADIR}/ruleCount_{TOOL}.csv")
    _ruleOccurrence_df['tool']=f'{TOOL}'
    ruleOccurrence = pd.concat([ruleOccurrence, _ruleOccurrence_df])

ruleOccurrence.rename(columns={'name':'ruleID'}, inplace=True)
ruleOccurrence.reset_index(inplace=True)

ruleOccurrence['resource'] = ruleOccurrence['ruleID'].map(errID2res_dict)
ruleOccurrence = ruleOccurrence.explode('resource')   # <><>< ????
# In[ RulesPerResource]

# | RESOURCE | OCCURENCES | TOOL |
# i.e. S3    |    6969    | tfsec

RulesPerResource = pd.DataFrame()

### We now need to group by items in the

for TOOL in TOOLS:
    _df = ruleOccurrence[ruleOccurrence['tool']==TOOL].groupby(['resource']).sum().reset_index()
    _df['tool']=TOOL
    RulesPerResource=pd.concat([RulesPerResource, _df], ignore_index=True)  # <><>

# Total rules
#for TOOL in TOOLS:
#    print(TOOL, len(ruleOccurrence[ruleOccurrence['tool']==TOOL]))

# In[ Something ]
xDF = {}

for TOOL in TOOLS:
    _xDF_df = pd.read_csv(f"{DATADIR}/ruleCount_{TOOL}.csv")
    _xDF_dict = dict(zip(_xDF_df.name, _xDF_df.occurences))
    xDF[f'n_{TOOL}'] = pd.Series(_xDF_dict, name=f'n_{TOOL}')

xDF = pd.concat([xDF['n_tfsec'], xDF['n_terrascan'], xDF['n_checkov']], axis=1)
xDF.fillna(0, inplace=True)
xDF['tool'] = xDF.idxmax(axis=1)
xDF.reset_index(inplace=True)
xDF.rename(columns={'index':'ruleID'}, inplace=True)

# This is not nice but it works:
resList = pd.Series([],dtype=pd.StringDtype())
for _, row in xDF.iterrows():
    rule = row['ruleID']
    resList = resList.append(res2errID[res2errID.allIDs.apply(lambda x: rule in x)].resource, ignore_index=True)
xDF['resource'] = resList

xDF = xDF.groupby(['resource']).sum()

resourceAllStats = res2errID.set_index('resource').join(xDF, on='resource')
resourceAllStats['n_all'] = resourceAllStats.apply(lambda row: row.n_tfsec + row.n_terrascan + row.n_checkov, axis=1)


for TOOL in TOOLS:
    resourceAllStats[f'n_{TOOL}_norm'] = resourceAllStats.apply(lambda row: row[f'n_{TOOL}']/row[f'N_{TOOL}'] if (row[f'N_{TOOL}']!=0 and not np.isnan(row[f'N_{TOOL}'])) else np.nan, axis=1)
    resourceAllStats[f'n_{TOOL}_norm'] = resourceAllStats[f'n_{TOOL}_norm']/resourceAllStats[f'n_{TOOL}_norm'].dropna().sum() * 100

resourceAllStats.fillna(0, inplace=True)

# Making the sum column
resourceAllStats['n_all_norm'] = resourceAllStats.n_tfsec_norm + resourceAllStats.n_terrascan_norm + resourceAllStats.n_checkov_norm
resourceAllStats['n_all_norm'] = resourceAllStats['n_all_norm'] / resourceAllStats['n_all_norm'].sum()*100

# In[ 1. -- PLOTTING - resource occurence percentages - top 15 overall]
#################################################################
##################### PLOTS START HERE ##########################
#################################################################
plotData = resourceOccurrence.copy()
plotData.sort_values('percentage', ascending=False, inplace=True)

SAVE = False
N    = 15


fig = plt.figure()
data   = plotData['percentage'][:N]
labels = plotData['name'].str.replace('aws_','')[:N]


colorLookup_resTypes = {'variable': 'blue',
                        'output'  : 'cyan',
                        'resource': 'red',
                        'data'    : 'green'
                       }

color = [colorLookup_resTypes[k] for k in plotData['type'][:N]]


legendHandles = []
for k,v in colorLookup_resTypes.items():
    legendHandles.append(mpatches.Patch(color=v, label=k))
# since we now have a legend, we can cut out the first word:
labels = [''.join(x.split(' ')[1:]) if ' ' in x else x for x in labels ]

plt.ylabel("Percentage")
plt.title(f"Used Resources - Top {N}")
plt.xticks(range(len(data)), labels, rotation=90)
plt.bar(range(len(data)), data, color=color)
plt.legend(handles=legendHandles)
if SAVE:
    plt.savefig(f'{FIGDIR}/Resources_Top{N}.pdf', bbox_inches = 'tight')

plt.show()
SAVE = False

# In[ 1a. 1b. -- PLOTTING - resource occurence percentages - top 15 in resTypes ]
resTypes = ['resource', 'data']

SAVE = False
N    = 15

for TYPE in resTypes:
    plotData = resourceOccurrence[resourceOccurrence['type']==TYPE].copy()
    # we need to re-normalize:
    reScale = plotData['percentage'].sum()/100
    plotData['percentage'] = plotData['percentage']/reScale
    plotData.sort_values('percentage', ascending=False, inplace=True)

    fig = plt.figure()
    data   = plotData['percentage'][:N]
    labels = plotData['name'].str.replace('aws_','')[:N]

    plt.ylabel("Percentage")
    plt.title(f"Distribution of '{TYPE}' types")
    plt.xticks(range(len(data)), labels, rotation=90)
    plt.bar(range(len(data)), data, color=[colorLookup[x] if x in colorLookup.keys() else 'lightcoral' if 'iam' in x else 'black' for x in labels])
    if SAVE:
        plt.savefig(f'{FIGDIR}/Resources_Top-{N}_{TYPE}.pdf', bbox_inches = 'tight')

    plt.show()
SAVE = False

# In[ 2. -- PLOTTING - most vulnerable resources, per tool ]
SAVE = False
N    = 10

#xxxxColorMap = set()
for TOOL in TOOLS:
    plotData = resourceAllStats.copy()
    plotData.sort_values(f'n_{TOOL}_norm', ascending=False, inplace=True)

    plotData.reset_index(inplace=True)

    fig = plt.figure()
    data   = plotData[f'n_{TOOL}_norm'][:N]
    labels = plotData['resource'].str.replace('aws_','')[:N]

    #xxxxColorMap.update(tuple(plotData['resource'][:N]))

    plt.ylabel("Percentage (normalized)")
    plt.title(f"{TOOL.capitalize()} - Top {N} vulnerable resources")
    plt.xticks(range(len(data)), labels, rotation=90)

    plt.bar(range(len(data)), data, color=[colorLookup[x] if x in colorLookup.keys() else 'lightcoral' if 'iam' in x else 'black' for x in labels])
    if SAVE:
        plt.savefig(f'{FIGDIR}/Resources_vulns_{TOOL}_Top{N}.pdf', bbox_inches = 'tight')

    plt.show()


SAVE = False


# In[ 3. -- PLOTTING most vulnerable overall (using n_all_norm col)]
SAVE = True
N    = 10

plotData = resourceAllStats.copy()
plotData.sort_values(f'n_all_norm', ascending=False, inplace=True)

plotData.reset_index(inplace=True)

fig = plt.figure()
data   = plotData[f'n_all_norm'][:N]
labels = plotData['resource'].str.replace('aws_','')[:N]

plt.ylabel("Percentage (normalized)")
plt.title(f"Top {N} vulnerable resources overall")
plt.xticks(range(len(data)), labels, rotation=90)

plt.bar(range(len(data)), data, color=[colorLookup[x] if x in colorLookup.keys() else 'lightcoral' if 'iam' in x else 'black' for x in labels])
if SAVE:
    plt.savefig(f'{FIGDIR}/resources_vulns_Top{N}.pdf', bbox_inches = 'tight')

plt.show()


SAVE = False

# In[ 4. -- PLOTTING - rules percentages, per tool  - just prep]
SAVE = True
N    = 10


#TODO: Label the bars
#TODO: Color by vuln category

# Used for removing confusion with errID's
label_prefix = {'checkov': 'CKV',
                'terrascan':'tscan',
                'tfsec':'tfsec'}

for TOOL in TOOLS:
    plotData = ruleOccurrence[ruleOccurrence['tool'] == TOOL].copy()
    plotData['percentage'] = plotData['occurences'] / plotData['occurences'].sum()*100
    plotData.sort_values('percentage', ascending=False, inplace=True)


    data   = plotData['percentage'][:N]
    labels = plotData['ruleID'][:N]
    labels_original = labels
    labels = labels.map(lambda x: re.findall(r'\d+',x)[-1].strip('0'))
    labels = [label_prefix[TOOL]+str(x) for x in labels]


    whatResource_barLabels = []
    for rule in labels_original:
        resource = res2errID[res2errID['allIDs'].apply(lambda x: rule in x)].resource.to_string(index=False).split('\n')[0]
        whatResource_barLabels.append(resource)

    # The 'else' comes from a deprecated thing:
    #  CKV_AWS_52: "Ensure S3 bucket has MFA delete enabled"
    whatResource_barLabels = [r.replace('aws_','').lstrip() if 'Series' not in r else 's3_bucket' for r in whatResource_barLabels]

    #print(whatResource_barLabels)
    color=[colorLookup[r] if r in colorLookup.keys() else 'black' for r in whatResource_barLabels ]
    #print(color, "\n\n")

    fig    = plt.figure()
    plt.ylabel("Percentage (normalized)")
    plt.title(f"Top {N} vulnerabilities - {TOOL}")
    plt.xticks(range(len(data)), labels, rotation=90)

    barPlot = plt.bar(range(len(data)), data, color=[colorLookup[r] if r in colorLookup.keys() else 'lightgray' for r in whatResource_barLabels ])
    labelTheBars(barPlot, whatResource_barLabels)

    if SAVE:
        plt.savefig(f'{FIGDIR}/Resources_Top-{N}_{TOOL}.pdf', bbox_inches = 'tight')

    plt.show()
SAVE = False


# In[ Latex top 5 vulnerable resources stat chart ]

# NOTE: the \sfrac{}{} thing requires \usepackage{xfrac}

# TODO: Take top 5 resources (Plot block 3.) and see what the top vulns on each look like
SAVE          = False
N_mostVulnRes = 5

selection = resourceAllStats.copy()
selection.sort_values(f'n_all_norm', ascending=False, inplace=True)
selection = selection[:N_mostVulnRes][['n_tfsec', 'n_terrascan', 'n_checkov', 'n_tfsec_norm', 'n_terrascan_norm', 'n_checkov_norm', 'N_tfsec', 'N_terrascan', 'N_checkov', 'n_all_norm', 'N_sum']]

for TOOL in TOOLS:
    #n = str(round(row[f'n_{TOOL}_norm'],2))
    #N = row[f'N_{TOOL}']
    selection[f'{TOOL}'] = selection.apply(lambda row: "$\sfrac{{{n}}}{{{N}}}$".format(n=round(row[f'n_{TOOL}_norm'],2), N=int(row[f'N_{TOOL}'])), axis=1)

selection['$\Sigma$'] = selection.apply(lambda row: "$\sfrac{{{n}}}{{{N}}}$".format(n=round(row[f'n_all_norm'],2), N=int(row[f'N_sum'])), axis=1)

selection = selection[:][TOOLS+['$\Sigma$']]

texExport = selection.to_latex(escape=False, column_format="|c|c|c|c|c|")

# Being lazy about find-and-replace
texExport = texExport.replace('midrule', 'hline').replace('bottomrule', 'hline').replace('toprule','hline\hline').replace('aws_', '').replace('_', '\_').replace('{}','')
texExport = '\\begin{table}\n'+texExport+'\end{table}'

mostVulnResNames = selection.index.values.tolist()

# In[ Now let's see top vulns for each ]:
selection = ruleOccurrence.copy()
selection = selection[selection['resource'].isin(mostVulnResNames)]

print(f'Most {N_mostVulnRes} resources: {mostVulnResNames}')

# Sorting desc, by tool, then reordering rows to keep the order of most vuln resources
selection = selection.sort_values(by=['tool', 'occurences'], ascending=[False, False])
selection.set_index('resource', inplace=True)
selection.drop(columns=['index'], inplace=True) # dropping old pointless index
selection = selection.loc[mostVulnResNames]

# TODO: Document what the top 5 resource types are, then go into top X vulns each
# Exporting to xlsx for this, and adding the error docstring by hand
selection.to_excel('topVulnResRules.xlsx')
