#!/bin/bash

# sort | uniq VS sort -u

IDlist=$(ls time | cut -d "_" -f 2 | sort -u)

for ID in $IDlist
do
    echo "${ID}..."
    allResourcesCount=$(cat what/$ID* | wc -l)
    allResourcesCountUniq=$(cat what/$ID* | sort | uniq | wc -l)
    echo "$allResourcesCount, $allResourcesCountUniq,  $(tail -n 1 time/time_${ID}*_checkov.txt)" 1>>time_checkov.csv 2>/dev/null

    subreportsCount=$(ls what/$ID* | wc -l)
    #echo $allResourcesCount $subreportsCount

    # format: time_ID_i_TOOL.txt
    for TOOL in tfsec terrascan
    do
        if [ $subreportsCount -ne 1 ]
        then
            for i in $(seq 1 $subreportsCount)
            do
                #${ID}_${i}
                echo "$(cat what/$ID_$i* | sort | uniq | wc -l), $(cat what/$ID_$i* | wc -l), $(tail -n 1 time/time_${ID}_${i}_$TOOL.txt)" 1>>time_${TOOL}.csv 2>/dev/null
            done
        else
            #${ID}
            echo "$(cat what/$ID* | sort | uniq | wc -l), $(cat what/$ID* | wc -l), $(tail -n 1 time/time_${ID}*_$TOOL.txt)" 1>>time_${TOOL}.csv 2>/dev/null
        fi
    done
done