#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plotting like there's no tomorrow
"""
from   datetime           import datetime
import matplotlib.patches as mpatches
import matplotlib.pyplot  as plt
import pandas             as pd
import numpy              as np
import subprocess
import json
import re
import os

# Hardcoding hacky one-and-done business
USER    = os.getenv('USER') #preserve multi-system compatibility
FIGDIR  = f'/home/{USER}/thesis_iosif/figures'
DATADIR = f'/home/{USER}/tf_sast_parsed'


SAVE = False

os.chdir(DATADIR)

def executeOS(cmd):
    x = subprocess.run([cmd], shell=True, capture_output=True, text=True)
    return x

def labelTheBars(barPlot, barLabels):
    '''
    Credit where credit is due:
    https://moonbooks.org/Articles/How-to-add-text-on-a-bar-with-matplotlib-/
    '''
    for idx,rect in enumerate(barPlot):
        #height = rect.get_height()
        plt.text(x=rect.get_x() + rect.get_width()/2., y=0.4,
                s=barLabels[idx],
                ha='center', va='baseline', rotation=90, color='black')

colorLookup = {'alb': 'darkgrey',
               'cloudtrail': 'gainsboro',
               'cloudwatch': 'peachpuff',
               'db_instance': 'darkorange',
               'iam_role_policy': 'lightcoral',
               'instance': 'yellow',
               'lambda_function': 'yellowgreen',
               'launch_configuration': 'palegreen',
               'launch_template': 'darkseagreen',
               's3_bucket': 'red',
               'security_group': 'darkcyan',
               'security_group_rule': 'blueviolet',
               'subnet': 'dodgerblue',
               'vpc': 'blue'}

# This will keep getting used throughout
res2errID = {}
for TOOL in ['tfsec', 'checkov', 'terrascan']:
    with open(f'/home/{USER}/thesis_iosif/resource2errID/resource2errID_{TOOL}.json', 'r') as f:
        res2errID[f'{TOOL}']=json.load(f)


df_forPlotting = []  # keeps what we're plotting, simplifies referring to that object later
keeperObjects = {}   # i.e. dataframes previously generated

# In[load data - all resource types]

df = pd.read_csv("resourceCount.csv")
df['name']=df['name'].str.strip() # 2 rows had trailing whitespace aaaaaaaaaaa
df['name']=df['name'].str.replace('aws_','') # 2 rows had trailing whitespace aaaaaaaaaaa

allSummed = sum(df.occurences.values)
df['percentage'] = df.apply(lambda row: round(row.occurences/allSummed*100, 2), axis=1)

# This is so hacky oooof
df_resOccurencePercent=df.copy()
df_resOccurencePercent['shortName']=df_resOccurencePercent['name'].apply(lambda x: x.split(' ')[-1] if ' ' in x else x)
dict_OccurencePercent = df_resOccurencePercent.set_index('shortName')['percentage'].to_dict()

keeperObjects['df_occurencePercent'] = df_resOccurencePercent
keeperObjects['dict_occurencePercent_shortName'] = dict_OccurencePercent


# In[Bar - all resource types]

N = 15
noOther = True

_df_temp = df[:N]
_df_temp = _df_temp.append({"occurences":sum(df.occurences.values[N:]),
                            "name": "other",
                            "percentage": sum(df.percentage.values[N:])},
                            ignore_index=True
                          )

df_forPlotting.append(_df_temp)


fig = plt.figure()
data   = df_forPlotting[-1]['percentage']
labels = [f"{x['name']}" for _, x in df_forPlotting[-1].iterrows()]

if noOther:
    data   = data[:-1]
    labels = labels[:-1]

colorLookup_resTypes = {'variable': 'blue',
                        'output':'cyan',
                        'resource':'red',
                        'data':'green'
                       }

color = [colorLookup_resTypes[k.split(' ')[0]] for k in labels]


legendHandles = []
for k,v in colorLookup_resTypes.items():
    legendHandles.append(mpatches.Patch(color=v, label=k))
# since we now have a legend, we can cut out the first word:
labels = [''.join(x.split(' ')[1:]) if ' ' in x else x for x in labels ]

plt.ylabel("Percentage")
plt.title(f"Used Resources - Top {N}")
plt.xticks(range(len(data)), labels, rotation=90)
plt.bar(range(len(data)), data, color=color)
plt.legend(handles=legendHandles)
if SAVE:
    plt.savefig(f'{FIGDIR}/Resources_top{N}used_withLegend.pdf', bbox_inches = 'tight')
plt.show()


### Variables: 6.32%, Outputs:5.03%  ----> probable unintended cleartext storage of secrets

# In[Pie/Bar - Used resources - distribution per keyword category]

#cmd = "cat resourceCount.csv | grep -oP \",(.+)[ |\n]\" | sed \"s/,//g\" | sort | uniq"
#resourceTypes = executeOS(cmd).stdout
#resourceTypes = resourceTypes.split('\n')

resourceTypes = ["Binary", "data", "output", "resource", "variable"]

N = 15

_Pie = False
_Bar = not _Pie


# Will be used later as a supra-normalization
distribution = {'data':None, 'resource': None}

for resType in resourceTypes:

    df_temp = df[df.name.str.startswith(resType)]

    if (len(df_temp)) > N:
        df_temp_subset = df_temp[:N]
        df_temp_subset = df_temp_subset.append({"occurences":sum(df_temp.occurences.values[N:]),
                                                 "name": "other",
                                                 "percentage": sum(df_temp.percentage.values[N:])},
                                                 ignore_index=True
                                                )
    else:
        # later consisteny of code
        df_temp_subset = df_temp

    # Global percentage is not subcategory percentage
    df_temp_subset = df_temp_subset.rename(columns={'percentage':'g_percentage'})
    allSummed = sum(df_temp_subset.occurences.values)
    df_temp_subset['percentage'] = df_temp_subset.apply(lambda row: round(row.occurences/allSummed*100, 2), axis=1)

    df_forPlotting.append(df_temp_subset)

    if _Pie:
        fig = plt.figure()
        plt.pie(df_forPlotting[-1]['percentage'], autopct='%1.0f%%')
        plt.legend(title = "Used Resources", labels=[f"{x['name']} ({x['percentage']}%)" for _, x in df_forPlotting[-1].iterrows()],
                   bbox_to_anchor=(1,1), loc='best')
        plt.tight_layout()

        plt.show()


    # Bar Charts Time for select data types
    if _Bar:
        if resType in ["data", "resource"]:
            fig = plt.figure()
            data   = df_forPlotting[-1]['percentage']
            labels = [f"{x['name'].replace(resType+' ', '').replace('aws_','')}" for _, x in df_forPlotting[-1].iterrows()]

            if noOther:
                data   = data[:-1]
                labels = labels[:-1]

            plt.ylabel("Percentage")
            plt.title(f"Distribution of '{resType}' types")
            plt.xticks(range(len(data)), labels, rotation=90)
            plt.bar(range(len(data)), data, color=[colorLookup[x] if x in colorLookup.keys() else 'lightcoral' if 'iam' in x else 'black' for x in labels])
            if SAVE:
                plt.savefig(f'{FIGDIR}/Resources_{resType}.pdf', bbox_inches = 'tight')
            plt.show()

# In[Pie/Bar per tool rules - data prep]

N = 10
label_prefix = {'checkov': 'CKV', 'terrascan':'tscan', 'tfsec':'tfsec'} # Used for removing confusion with errID's

# xTMP_resSet = set()  ### used for building colorLookup


xxJoinDF = pd.DataFrame()

for tool in ['tfsec', 'terrascan', 'checkov']:
    df = pd.read_csv(f"ruleCount_{tool}.csv")

    allSummed = sum(df.occurences.values)

    # Make percentage nicer to display
    df['percentage'] = df.apply(lambda row: round(row.occurences/allSummed*100, 2), axis=1)
    keeperObjects[f"{tool}_rules2percent"] = df

    # Map the errorID to a resource (opposite of what res2errID is holding)
    _id2resource = {}
    for res, idList in res2errID[tool].items():
        for _id in idList:
            _id2resource[_id] = res
    df['resource']   = df['name'].map(_id2resource)


    df_resource2errPercent = df[['resource', 'percentage', 'occurences']].groupby('resource').sum().sort_values('percentage', ascending=False).reset_index()

    keeperObjects[f"{tool}_resources2percent"] = df_resource2errPercent
    keeperObjects[f'{tool}_resources2percent']['tool'] = tool
    xxJoinDF = xxJoinDF.append(keeperObjects[f'{tool}_resources2percent'], ignore_index=True)

    if (len(df)) > N:
        df_subset = df[:N]
        df_subset = df_subset.append({"occurences":sum(df.occurences.values[N:]),
                                      "name": "other",
                                      "percentage": sum(df.percentage.values[N:])},
                                     ignore_index=True
                                    )

    df_forPlotting.append(df_subset)

# In[]
xxSUMAVG = pd.DataFrame({'sumOccurences': xxJoinDF.groupby('resource')['occurences'].sum(),
                         'mean': xxJoinDF.groupby('resource')['percentage'].mean()})

#xxSUM = xxJoinDF.groupby('resource')['occurences'].sum()
#xxSUM = xxJoinDF.groupby('resource')['percentage'].mean()


# In[Pie/Bar per tool rules - plotting]
for tool in ['tfsec', 'terrascan', 'checkov']:
    _Pie = False
    _Bar = not _Pie

    if _Pie:
        fig = plt.figure()
        plt.pie(df_forPlotting[-1]['percentage'], autopct='%1.0f%%')
        plt.legend(title = f"Top {tool} violations", labels=[f"{x['name']} ({x['percentage']}%)" for _, x in df_forPlotting[-1].iterrows()],
                   bbox_to_anchor=(1,1), loc='best')
        plt.tight_layout()

        plt.show()

    if _Bar:

        data   = df_forPlotting[-1]['percentage']
        labels = df_forPlotting[-1]['name']

        if noOther:
            data   = data[:-1]
            labels = labels[:-1]

        whatResource_barLabels = []
        for rule in labels:
            for resource, idList in res2errID[tool].items():
                if rule in idList:
                    whatResource_barLabels.append(resource.replace('aws_',''))
                    # xTMP_resSet.add(resource.replace('aws_',''))   ### used for building colorLookup
        ### ----
        ### Plot for top N Error ID's per tool
        ### ---
        plt.figure()
        plt.ylabel("Global Percentage")
        plt.xlabel("Error ID")

        # Shorten the error labels to just the error ID
        labels = labels.map(lambda x: re.findall(r'\d+',x)[-1].strip('0'))
        labels = [label_prefix[tool]+str(x) for x in labels]


        plt.xticks(range(len(data)), labels, rotation=60)
        barPlot = plt.bar(range(len(data)), data, color=[colorLookup[r] for r in whatResource_barLabels])
        labelTheBars(barPlot, whatResource_barLabels)

        plt.title(f"{tool.capitalize()} - Top {N} vulnerabilities overall")
        if SAVE:
            plt.savefig(f'{FIGDIR}/{tool}_top{N}vulns_colored.pdf', bbox_inches = 'tight')

        plt.show()

# In[Bar for top N vulnerable resources]
N = 15
SAVE = False

# Do we want to normalize based on the number of available rules for each resource?
_norm = True

for tool in ['tfsec', 'terrascan', 'checkov']:
    ### ----
    ### Plot for top N vulnerable resources per tool
    ### ---

    df = keeperObjects[f'{tool}_resources2percent']


    # Normalize percent based on how many rules per resource there are --> "equalize tools"
    if _norm:
        normFactor = {}
        for res, errIDlist in res2errID[tool].items():
            normFactor[res] = len(errIDlist)

        # divide by numer of rules available for each resource
        df['percentage'] = df.apply(lambda x: x.percentage/normFactor[x.resource], axis=1)
        # divide by sum of current percentage column, to ensure stuff adds up to 100%
        df['percentage'] = df['percentage']/(df['percentage'].sum()/100)
        df.sort_values('percentage', ascending=False, inplace=True)
        df.reset_index(inplace=True)



    data   = df['percentage'][:N]
    labels = df['resource'].str.replace('aws_','')[:N]

    plt.figure()
    if _norm:
        plt.title(f"{tool.capitalize()} - Top {N} vulnerable resources (normalised)")
    else:
        plt.title(f"{tool.capitalize()} - Top {N} vulnerable resources")
    plt.ylabel("Global Percentage")
    plt.xlabel("Resource")
    plt.xticks(range(len(data)), labels, rotation=90)

    colors = [colorLookup[r] if r in colorLookup.keys() else 'lightcoral' if 'iam' in r else 'black' for r in labels]
    plt.bar(range(len(data)), data, color=colors)

    if SAVE:
        if _norm:
            plt.savefig(f"{FIGDIR}/resources_top{N}_{tool}_normX.pdf", bbox_inches='tight', dpi=600, format='pdf')
        else:
            plt.savefig(f"{FIGDIR}/resources_top{N}_{tool}X.pdf", bbox_inches='tight', dpi=600, format='pdf')
    plt.show()

SAVE = False
# In[time/resource]

os.chdir(f"/home/{USER}/thesis_iosif")


# Note on the Uniq thing: Some repos have the same kind of resource, multiple times. Hence, wanted to see if unique makes a difference or not.
# Turns out, it doesn't ---> Which to take? (Q!!)
timeDf_res = pd.DataFrame()
timeDf_resUniq = pd.DataFrame()

for TOOL in ['tfsec', 'terrascan', 'checkov']:
    # print(f"---{TOOL}---")
    df=pd.read_csv(f"data/time/time_{TOOL}.csv", skipinitialspace=True)
    df.dropna(inplace=True)
    df['time'] = df['time'].apply(lambda x: 60*float(x.split(':')[-2])+float(x.split(':')[-1]))

    averagedResTypes = df[['resTypes', 'time']].groupby('resTypes').mean()#.rename(columns={'time': f'time ({TOOL})'})
    averagedResTypesUniq = df[['resTypesUniq', 'time']].groupby('resTypesUniq').mean()#.rename(columns={'time': f'time ({TOOL})'})

    timeDf_res[f"{TOOL}"] = averagedResTypes['time']
    timeDf_resUniq[f"{TOOL}"] = averagedResTypesUniq['time']

    ax = averagedResTypes.plot.bar(y='time', rot=90, title=f"{TOOL}: time (s.)")
    ax.xaxis.set_major_locator(plt.MaxNLocator(prune='both'))

    if SAVE:
        ax.figure.savefig(f'{FIGDIR}/time_{TOOL}.pdf', bbox_inches = 'tight')



    ax = averagedResTypesUniq.plot.bar(y='time', rot=90, title=f"{TOOL}: time (s.)")
    ax.xaxis.set_major_locator(plt.MaxNLocator(prune='both'))

    if SAVE:
        ax.figure.savefig(f'{FIGDIR}/time_{TOOL}_uniq.pdf', bbox_inches = 'tight')

# In[time/resource(stacked)]

timeDf_res.dropna(inplace=True)
timeDf_resUniq.dropna(inplace=True)

ax = timeDf_res.plot(kind="bar")
ax.xaxis.set_major_locator(plt.MaxNLocator(prune='both'))
plt.title("Execution time")
plt.xlabel("Resource Count")
plt.ylabel("time (s.)")
if SAVE:
    ax.figure.savefig(f'{FIGDIR}/time_allThree.pdf', bbox_inches = 'tight')

ax = timeDf_resUniq.plot(kind="bar")
ax.xaxis.set_major_locator(plt.MaxNLocator(prune='both'))
plt.title("Execution time")
plt.xlabel("Unique Resource Count")
plt.ylabel("time (s.)")
if SAVE:
    ax.figure.savefig(f'{FIGDIR}/time_allThree_uniq.pdf', bbox_inches = 'tight')

# In[time(between tools GLOBAL)]

avgTime = timeDf_res.mean()
ax = avgTime.plot(kind="bar", rot="0", logy=True)
plt.title("Execution time - Global Average")
plt.ylabel("time (s.) - log")
if SAVE:
    ax.figure.savefig(f'{FIGDIR}/time_allThreeComparedGlobal.pdf', bbox_inches = 'tight')



# In[errors/resource normalized by tool's available error scans for that tool]
# Values >1 because we didn't count the number of occurences. Q!!
#TODO: We can also 'normalize' by multiplying with the percentage from % used resources.

#TODO: make the folder structure more intuitive :c
#TODO: expand colorLookup for this
os.chdir(f'/home/{USER}/thesis_iosif')
violations = {}

# This will hold "how many errID's per resouce are aavailable for each tool
res2errID_totalCount = {'tfsec'    : {},
                        'checkov'  : {},
                        'terrascan': {}
                       }


# This will hold what we're plotting
violationTotal = {'tfsec'    : {},
                  'checkov'  : {},
                  'terrascan': {}
                 }

violationTotal_norm = {'tfsec'    : {},
                       'checkov'  : {},
                       'terrascan': {}
                      }

for TOOL in ['tfsec', 'checkov', 'terrascan']:
    for k,v in res2errID[f'{TOOL}'].items():
        res2errID_totalCount[f'{TOOL}'][k]=len(v)
    with open(f'_intermResults/ruleCount_{TOOL}.csv', 'r') as f:
        violations[f'{TOOL}']=pd.read_csv(f)



#### Bar Chart 1: #vulns/resource PER tool
for TOOL in ['tfsec']:#, 'checkov', 'terrascan']:
    _res2errID = res2errID[TOOL]
    _res2errID_totalCount = res2errID_totalCount[TOOL]
    _violations = violations[TOOL]

    for res, res_errIDlist in list(_res2errID.items()):

        # get total occurences for all violations for one resource
        df_1res = _violations[_violations['name'].isin(res_errIDlist)]

        # compute total, and normalized total
        total = sum(df_1res['occurences'])
        total_norm = total / _res2errID_totalCount[res]

        violationTotal[TOOL][res] = total
        violationTotal_norm[TOOL][res] = total_norm

for TOOL in ['tfsec']:#, 'checkov', 'terrascan']:


    # SORT DESC
    violationTotal[TOOL] = {key: value for key, value in sorted(violationTotal[TOOL].items(), key=lambda item: item[1], reverse=True)}
    violationTotal_norm[TOOL]= {key: value for key, value in sorted(violationTotal_norm[TOOL].items(), key=lambda item: item[1], reverse=True)}

    N = 15

    data   = list(violationTotal[TOOL].values())[:N]
    labels = [x.replace('aws_','') for x in list(violationTotal[TOOL].keys())[:N]]

    fig = plt.figure()
    plt.ylabel("# Violations")
    plt.title(f"Violations per resource: {TOOL} top {N}")
    plt.xticks(range(len(data)), labels, rotation=90)
    plt.bar(range(len(data)), data)

    if SAVE:
        plt.savefig(f'{FIGDIR}/violations_perRes_{TOOL}.pdf', bbox_inches = 'tight')
    plt.show()

    continue
    # ---- and now normalized

    data   = list(violationTotal_norm[TOOL].values())[:N]
    labels = [x.replace('aws_','') for x in list(violationTotal_norm[TOOL].keys())[:N]]

    fig = plt.figure()
    plt.ylabel("# Violations")
    plt.title(f"Violations per resource: {TOOL} top {N} (normalized)")
    plt.xticks(range(len(data)), labels, rotation=90)
    plt.bar(range(len(data)), data)


    if SAVE:
        plt.savefig(f'{FIGDIR}/violations_perRes_norm_{TOOL}.pdf', bbox_inches = 'tight')
    plt.show()


    # ---- and now normalized DOUBLETIME

    #For double normalizing, we need the appearance percentages stored in dict_resOccurencePercent
    for k,v in violationTotal_norm[TOOL].items():
        k_short = k.replace('aws_', '')
        if k_short in dict_OccurencePercent.keys():
            percent = dict_OccurencePercent[k_short] / 100
            if percent > 0:
                violationTotal_norm[TOOL][k] = violationTotal_norm[TOOL][k] * percent
        else:
            violationTotal_norm[TOOL][k] = -99
            '''
            tfsec: provider
            terrascan: aws_cloudwatch
            terrascan: aws_config
            terrascan: aws_elasticcache_replication_group
            checkov: aws_timestreamwrite_database
            checkov: aws_qldb_ledger
            '''

    # there's a handful of missing vals we're trimming out
    violationTotal_norm[TOOL] = {k:v for k,v in violationTotal_norm[TOOL].items() if v!=-99}

    # Sort again
    violationTotal_norm[TOOL]= {key: value for key, value in sorted(violationTotal_norm[TOOL].items(), key=lambda item: item[1], reverse=True)}

    data   = list(violationTotal_norm[TOOL].values())[:N]    #<-- add doubleNorm here

    labels = [x.replace('aws_','') for x in list(violationTotal_norm[TOOL].keys())[:N]]

    fig = plt.figure()
    plt.ylabel("# Violations")
    plt.title(f"Violations per resource: {TOOL} top {N} (normalized x2)")
    plt.xticks(range(len(data)), labels, rotation=90)
    plt.bar(range(len(data)), data)


    if SAVE:
        plt.savefig(f'{FIGDIR}/violations_perRes_norm_x2_{TOOL}.pdf', bbox_inches = 'tight')
    plt.show()


# In[norm x2 and averaged]: --- DOES NOT WORK...
avgNormViolations = {}
for resType in violationTotal_norm['tfsec'].keys():
    try:
        avgNormViolations[resType] = (violationTotal_norm['tfsec'][resType] + violationTotal_norm['terrascan'][resType] + violationTotal_norm['checkov'][resType])/3
    except:
        pass

avgNormViolations= {key: value for key, value in sorted(avgNormViolations.items(), key=lambda item: item[1], reverse=True)}
data   = list(avgNormViolations.values())[:N]    #<-- add doubleNorm here

labels = [x.replace('','') for x in list(avgNormViolations.keys())[:N]]

fig = plt.figure()
plt.ylabel("# Violations")
plt.title(f"Violations per resource: {TOOL} top {N} (normalized and averaged)")
plt.xticks(range(len(data)), labels, rotation=90)
plt.bar(range(len(data)), data)


if SAVE:
    plt.savefig(f'{FIGDIR}/violations_perRes_norm_{TOOL}.pdf', bbox_inches = 'tight')
plt.show()



# In[]
# Hash table for Fig
