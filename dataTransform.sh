#!/bin/bash

PARSED_DIR=$HOME/tf_sast_parsed

######################
# Counting Resources
######################
cat $PARSED_DIR/2*/*.json | jq .resources | sed 's/,//g' | sort |uniq -c | sort -nr | tr -s ' ' | awk '{print $1","$2" "$3}'  | sed "s/[\\\"|\\]//g" > $PARSED_DIR/tmpfile

# removing stray '[' and ']' items
cat $PARSED_DIR/tmpfile | awk 'NR > 2 {print}' > $PARSED_DIR/resourceCount.csv && rm $PARSED_DIR/tmpfile

# adding a header
sed -i '1s/^/occurences,name\n/' $PARSED_DIR/resourceCount.csv

for tool in tfsec terrascan checkov
do
  cat $PARSED_DIR/2*/*.json | jq ".$tool[]|.rule_id" | sort | uniq -c | sort -nr | sed "s/\"//g" | tr -s ' ' | awk '{print$1","$2}' > $PARSED_DIR/ruleCount_$tool.csv
  sed -i '1s/^/occurences,name\n/' $PARSED_DIR/ruleCount_$tool.csv
done
