#!/usr/bin/env python3

import pandas
import numpy as np

resultsFileName = "tools.csv"

results = pandas.read_csv(resultsFileName)
#%%
r = results.groupby(["resource","tool_name"]).size().sort_values(ascending=False).reset_index()
x = r.reindex(columns=["tool_name","resource",0]).sort_values(by=["tool_name",0],ascending=False)

#%%
r = results.groupby(["tool_name","repo_id"])['rule_id'].nunique().sort_values(ascending=False).reset_index()
x = r.reindex(columns=["tool_name","repo_id","rule_id"]).sort_values(by=["tool_name","rule_id"],ascending=False)
y = r.reindex(columns=["tool_name","rule_id"]).value_counts(normalize=False).sort_values(ascending=False).reset_index()
#%
y = y.sort_values(by=['tool_name','rule_id'],ascending=True)
ysum = y.groupby(["tool_name"]).sum()[0]
