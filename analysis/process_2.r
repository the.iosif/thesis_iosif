# install.packages("tidyverse")
# install.packages("chron")

library(tidyverse)
library(ggplot2)
library(chron)
rm(list=ls())
dev.off(dev.list()["RStudioGD"])

results <- read_csv("results.csv") %>%
              mutate( rule_id = na_if(rule_id, "NO_RULE_ID"))     #  make NO_RULE_ID to NA

r <- results %>%
  select(tool_name,repo_id,rule_id, updated_at) %>%
  mutate(date = as.chron(updated_at))%>%
  mutate(year = years(date), month = as.numeric( months(date) ) ) %>%
  filter( !is.na(rule_id) ) %>%
  select(tool_name, repo_id, rule_id, year, month) %>%
  group_by(tool_name, repo_id, rule_id) %>% distinct() %>% ungroup()

# Number of vulnerabilities over time
aa <- r %>%
  group_by(year) %>%
  count()

# Number of repositories per year
bb <- r %>% ungroup %>%
  select(year, repo_id) %>% distinct() %>% 
  group_by(year) %>% count()

cc <- inner_join(aa,bb,by=c("year")) %>%
  rename(Violations=n.x, Repositories=n.y) %>%
  pivot_longer(cols=2:3)

#cc %>% ggplot(
#    aes(x=year, y=value, group=name)
#  ) +
#  geom_bar(stat="identity",position = position_dodge(width = 0.8)) +
#  facet_wrap(~name, ncol=1, scales="free_y") +
#  theme_bw()
#ggsave("cc.pdf", plot = last_plot(), device = "pdf",
#       scale = 1, height = 6, width = 9, units = "cm",
#       dpi = 300)

dd <- inner_join(aa,bb,by=c("year")) %>%
  rename(nrFails=n.x, nrRepos=n.y) %>%
  mutate( AvgViolationsPerRepo = nrFails/nrRepos )  %>%
  pivot_longer(cols=2:4)

dd %>% ggplot(
  aes(x=year, y=value, group=name) ) +
  geom_bar(stat="identity",position = position_dodge(width = 0.8)) +
  facet_wrap(~name, ncol=1, scales="free_y") +
  ylab('Count')+
  theme_bw()
ggsave("yearly.pdf", plot = last_plot(), device = "pdf",
       scale = 1, height = 6, width = 9, units = "cm",
       dpi = 300)


