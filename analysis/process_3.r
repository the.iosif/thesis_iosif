# install.packages("tidyverse")
# install.packages("xtable")

library(tidyverse)
library(ggplot2)
rm(list=ls())
dev.off(dev.list()["RStudioGD"])

results <- read_csv("results.csv") %>%
  mutate( rule_id = na_if(rule_id, "NO_RULE_ID"))     #  make NO_RULE_ID to NA

r <- results %>%
  mutate(date = as.chron(updated_at))%>%
  mutate(year = years(date), month = as.numeric( months(date) ) ) %>%
  select(tool_name,repo_id,rule_id,severity,resource, year)

### Total Severity
aa <- r %>%
  filter(!is.na(severity))

bb <- aa %>%
  group_by(severity) %>%
  count()
bb

### Severity by Tool
cc <- aa %>%
  group_by(tool_name, severity) %>%
  count()
cc

### Severity be Resource
dd <- aa %>%
  group_by(resource,severity) %>%
  count()

## Severity over Time
ee <- aa %>%
  group_by(year,severity) %>%
  count()

ee %>% filter(severity!="INFO") %>% ggplot(
  aes(x=year, y=n, group=severity)) +
  geom_line( aes(linetype=severity) ) +
  geom_point( aes(shape=severity) ) +
  theme(legend.position = c(0.15, 0.815)) +
  xlab("Year") +
  ylab("Findings Count") + scale_y_continuous(trans='log10')
ggsave("yearlySeverity.pdf", plot = last_plot(), device = "pdf",
       scale = 1, height = 12, width = 12, units = "cm",
       dpi = 300)