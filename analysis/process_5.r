#sudo apt install libudunits2-dev libmagick++-dev libgdal-dev -y 

#install.packages('ggplot2')
#install.packages('scales')
#install.packages('grid')
#install.packages('glue')
#install.packages('rlang')
#install.packages('sf')
#install.packages('png')

# install.packages("remotes")
#remotes::install_github("trevorld/gridpattern")
#remotes::install_github("coolbutuseless/ggpattern")

library(tidyverse)
library(ggplot2)
library(ggpattern)

# Got this from pandas, data was there already
results <- read_csv("THIS.csv") %>%
  arrange(desc(percentage))

results %>%
  ggplot(
    aes(Type, x=reorder(name, -percentage), y=percentage),
    pattern=Type, pattern_angle=Type, pattern_spacing=Type)+
  geom_bar_pattern(stat="identity", position=position_dodge(),
                   pattern_density = 0.025,
                   #pattern_angle=45,
                   pattern_color = "white",
                   pattern_fill = "black",
                   aes(pattern=Type)) +
  xlab("Resource")+
  ylab("Percentage")+
  scale_pattern_spacing_discrete(range = c(0.01, 0.05)) +
  theme(axis.text.x = element_text(angle = 60, vjust = 0.99, hjust=1))+
  theme(legend.position = c(0.82, 0.7))

ggsave("ResTypes.pdf", plot = last_plot(), device = "pdf",
       scale = 1, height = 12, width = 12, units = "cm",
       dpi = 300)