# install.packages("tidyverse")
# install.packages("xtable")

library(tidyverse)
library(dplyr)
library(ggplot2)

setwd("/home/j/thesis_iosif/analysis")

rm(list=ls())
dev.off(dev.list()["RStudioGD"])

results <- read_csv("results.csv") %>%
  mutate( rule_id = na_if(rule_id, "NO_RULE_ID"))     #  make NO_RULE_ID to NA

r <- results %>%
  mutate(date = as.chron(updated_at))%>%
  mutate(year = years(date), month = as.numeric( months(date) ) ) %>%
  select(tool_name,repo_id,rule_id,severity,resource, year, forks_count)


aa <- r %>%
  select(repo_id,forks_count,tool_name,rule_id,year) %>%
  group_by(repo_id,rule_id) %>%
  mutate(n_rule=n()) %>%
  distinct() %>%
  ungroup()

bb <- aa %>%
  group_by(repo_id,forks_count,tool_name,year) %>%
  mutate(nFails=sum(n_rule)) %>%
  select(repo_id,forks_count,tool_name,year,nFails) %>%
  distinct()

cc <- bb %>%
  #group_by(forks_count,tool_name,year) %>%
  group_by(forks_count,year) %>%
  mutate(avgViolations=mean(nFails))

dd <- cc %>%
  #select(forks_count,tool_name,year,avgViolations) %>%
  select(forks_count,year,avgViolations) %>%
  distinct()

ee <- dd %>%
  filter(forks_count<10) %>%
  arrange(year,forks_count)

ee %>% ggplot(
  aes(x=forks_count, y=avgViolations, group=year)) +
  geom_line( aes(linetype=year, color=year) ) +
  geom_point( aes(shape=year, color=year) ) +
  theme(legend.position = c(0.13, 0.70))+
  labs(linetype='meow')

ee %>% ggplot(
  aes(x=year, y=avgViolations, group=factor(forks_count))) +
  geom_line( aes(linetype=factor(forks_count), color=factor(forks_count)) ) +
  geom_point( aes(shape=factor(forks_count), color=factor(forks_count)) ) +
  theme(legend.position = c(0.13, 0.70))

