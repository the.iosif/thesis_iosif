import glob
import csv

allCSVFiles = glob.glob("raw/tf_links/*allStats.csv")
outFileName = "metadata.csv"

allRepos  = {}
allData   = []
allFields = []
for fileName in allCSVFiles:
    allRows = []
    with open(fileName) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for csvRow in csvReader:
            allRows.append(csvRow)
        if ( not(allFields==[]) and not(allFields==allRows[0]) ):
            print("PROBLEM!!!")
            break
        allFields = allRows[0]
        if len(allRows)>1:
            for row in allRows[1:]:
                repo_id = row[0]
                if repo_id in allRepos:
                    if not(allRepos[repo_id] == row):
                        print("PROBLEM -- ",repo_id)
                        print("           ",allRepos[repo_id])
                        print("           ",row)
                        break
                allRepos[repo_id] = row
                allData.append(row)

with open(outFileName, 'w', newline='') as fObj:
    writer = csv.writer(fObj)
    writer.writerow(allFields)
    writer.writerows(allData)
