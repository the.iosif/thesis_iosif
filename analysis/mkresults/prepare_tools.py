#!/usr/bin/env python3

from pathlib import Path
import pprint as pp
import json
import sys
import csv

outFileName = "tools.csv"
allFields   = ['repo_id',"full_name","created_at","updated_at","stargazers_count","fork","forks_count", 'tool_name','rule_id','resource','guideline','severity']
noRuleID    = "NO_RULE_ID"

def readMetadata(fileName, asDict=False):
    allRows = []
    with open(fileName) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for csvRow in csvReader:
            allRows.append(csvRow)
    header = allRows[0]
    if not(header == ["id","full_name","created_at","updated_at","stargazers_count","fork","forks_count"]):
        print("FORMAT ERROR: metadata.csv")
        sys.exit(0)
    if asDict:
        d = {}
        for r in allRows[1:]:
            d[r[0]] = r
        return d
    else:
        return allRows[1:]

# for this function, metadata must be a list of lists
def getRepoId(metadata,repoID):
    for row in metadata:
        if row[0]==repoID:
            return row
    return None

def loadJSON(fileName):
    try:
        with open(fileName) as json_file:
             result = json.load(json_file)
    except:
        result = {}
    return result

def normalizeCheckov(d,repo_info):
    _normFails = []
    if type(d)==dict:
        allFails = d["results"]["failed_checks"]
        if allFails==[]:
                return None
        for fail in allFails:
            ruleID    = fail["check_id"]
            if ruleID[:8]=="CKV_AWS_":
                resource      = fail["resource"].split(".")[0]
                guideline     = fail.get("guideline",None)
                severity      = None
                newRow        = list(repo_info)
                newRow.extend(['checkov',ruleID,resource,guideline,severity])
                _normFails.append(newRow)
        if _normFails==[]:
                return None
        return _normFails
    elif type(d)==list:
        for x in d:
            if x["check_type"]=="terraform":
                norm = normalizeCheckov(x,repo_info) 
                if norm:
                    _normFails.extend( norm )
        if _normFails==[]:
            return None
        else:
            return _normFails
    print("HELP ME")
    sys.exit(0)

def normalizeTerrascan(d,repo_info):
    _normFails = []
    allFails = d["results"]["violations"]
    if allFails==None:
        return None
    for fail in allFails:
        ruleID    = fail["rule_id"]
        resource  = fail["resource_type"]
        if resource[:4]=="aws_":
            guideline = None
            severity  = fail["severity"]
            newRow    = list(repo_info)
            newRow.extend(['terrascan',ruleID,resource,guideline,severity])
            _normFails.append(newRow)
    if _normFails==[]:
        return None
    else:
        return _normFails

def getTfsecResouce(s):
    # Resouce '<resource>' ...
    if s[:10]=="Resource '":                  return s[10:].split(".")[0]
    # Resouce <resource> ...
    if s[:9]=="Resource ":                    return s[9:].split(".")[0]
    # Variable '...
    if s[:10]=="Variable '":                  return "variable"
    # Block '...
    if s[:7]=="Block '":                      return "block"
    # Provider '...
    if s[:10]=="Provider '":                  return "provider"
    # Locals '...
    if s[:7]=="Local '":                      return "local"
    # 'module
    if s[:7]=="'module":                      return "module"
    # SQS policy '
    if s[:12]=="SQS policy '":                return "aws_sqs_queue_policy"
    # DAX cluster '
    if s[:13]=="DAX cluster '":               return "aws_dax_cluster"
    # CodeBuild project '
    if s[:19]=="CodeBuild project '":         return "aws_codebuild_project"
    print("HERE: ",s)
    sys.exit(0)

def normalizeTfsec(d,repo_info):
    _normFails = []
    allFails = d["results"]
    if allFails==None:
        return None
    flagFoundFail= False
    for fail in allFails:
        ruleProvider = fail["rule_provider"]
        if ruleProvider=="aws":
            ruleID        = fail["rule_id"]
            resource      = getTfsecResouce(fail["description"])
            guideline     = "https://tfsec.dev/docs/aws/"+ruleID
            severity      = fail["severity"]
            newRow        = list(repo_info)
            flagFoundFail = True
            newRow.extend(['tfsec',ruleID,resource,guideline,severity])
            _normFails.append(newRow)
    if flagFoundFail==False:
        return None
    return _normFails


##############################################################################
##############################################################################
##############################################################################

normFunction = {  "checkov"   : normalizeCheckov,
                  "terrascan" : normalizeTerrascan,
                  "tfsec"     : normalizeTfsec
               }

myMetadata = readMetadata("metadata.csv",True)
allPaths   = []
for path in Path('raw/tf_sast/').rglob('*.stdout'):
    allPaths.extend([str(path)])
allPaths.sort()
repos = {}

for path in allPaths:
    repoID         = path.split("/")[-1]
    _repoID        = repoID.split("_")[0].split(".")[0]
    tool           = path.split("/")[3]
    repos[_repoID] = repos.get( _repoID, {'checkov':[], 'terrascan':[], 'tfsec':[]} )
    repos[_repoID][tool].append(path)

n = 0
allNormFails = []
for repo in  repos:
    metadata = myMetadata[repo]
    for tool in ['checkov', 'terrascan', 'tfsec']:
        n += 1
        if n>1450000: break
        allFindingsTool = []
        for p in repos[repo][tool]:
            print(n, repo, tool, p)
            readJson   = loadJSON(p)
            if not(readJson=={}):
                normalized = normFunction[tool](readJson, metadata)
                if normalized:
                    allFindingsTool.extend(normalized)
        if allFindingsTool==[]:
            newRow    = list(metadata)
            newRow.extend([tool,noRuleID,None,None,None])
            allNormFails.append(newRow)
        else:
            allNormFails.extend(allFindingsTool)

if False:
    with open(outFileName, 'w', newline='') as fObj:
        writer = csv.writer(fObj)
        writer.writerow(allFields)
        writer.writerows(allNormFails)

