#!/usr/bin/env python

from   pathlib import Path
import pprint as pp
import json
import sys
import csv

inFileName  = "tools.csv"
outFileName = "tools_fix.csv"

checkokSeverity = {}
with open("checkov_all.csv") as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    for csvRow in csvReader:
        checkokSeverity[csvRow[0]] = csvRow[1]

allRows = []
with open(inFileName) as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    for csvRow in csvReader:
        allRows.append(csvRow)
header  = allRows[0]
allRows = allRows[1:]

for row in allRows:
    rule_id = row[-4]
    if rule_id in checkokSeverity:
        row[-1] = checkokSeverity[ rule_id ]
    if row[-1]: row[-1] = row[-1].upper()

allFields = header
if True:
    with open(outFileName, 'w', newline='') as fObj:
        writer = csv.writer(fObj)
        writer.writerow(allFields)
        writer.writerows(allRows)

