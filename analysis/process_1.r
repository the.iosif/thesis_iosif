# install.packages("tidyverse")

library(tidyverse)
library(ggplot2)
rm(list=ls())
dev.off(dev.list()["RStudioGD"])

results <- read_csv("results.csv") %>%
              mutate( rule_id = na_if(rule_id, "NO_RULE_ID"))     #  make NO_RULE_ID to NA

# How many Different Repositories?
p <- results      %>%
  select(repo_id) %>%
  distinct_all()  %>%
  count()

# Top Used Resources
res <- results %>%
  drop_na(resource) %>%
  group_by(resource) %>%
  count(resource) %>%
  arrange(-n)

head(res, n=15) %>%
  ggplot(
  aes(reorder(resource, -n), y=n/sum(n)))+
  geom_bar(stat="identity", position=position_dodge()) +
  xlab("Resource")+
  theme(axis.text.x = element_text(angle = 60, vjust = 0.99, hjust=1), axis.title.y = element_text(hjust=0), axis.title.x = element_text(hjust=0.4))

# Top-n Instances with Vulnerabilities
x <- results %>% 
  group_by(tool_name) %>%
  drop_na(resource)   %>%
  count(resource)     %>%
  group_by(tool_name) %>%
  arrange(-n, .by_group = TRUE)

top <- x %>%
  group_by(tool_name) %>%
  top_n(6)

top %>% ggplot(
  aes(x=reorder(resource, -n), y=n/sum(n))) +
  geom_bar(stat="identity", position=position_dodge()) +
  xlab("Resource")+
  ylab("Percentage (normalised)")+
  theme(axis.text.x = element_text(angle = 60, vjust = 0.99, hjust=1), axis.title.y = element_text(hjust=0), axis.title.x = element_text(hjust=0.4))
ggsave("TopResources.pdf", plot = last_plot(), device = "pdf",
       scale = 1, height = 9, width = 9, units = "cm",
       dpi = 300)

# Probability of <n unique findings per tool
r <- results %>% select(tool_name,repo_id,rule_id)

y <- r %>% filter(!is.na(rule_id)) %>%
  #mutate(rule_id = replace_na(rule_id,0))  %>%
  group_by(tool_name,repo_id) %>%
  #count(name="nrFail")
  summarize(nrFail = n_distinct(rule_id))

yNA <- r %>% filter(is.na(rule_id)) %>%
  group_by(tool_name,repo_id) %>%
  #count(name="nrFail")
  summarize(nrFail = n_distinct(rule_id))


z <- y %>%
  group_by(tool_name,nrFail) %>%
  count(nrFail)
zNA <- yNA %>%
  group_by(tool_name,nrFail) %>%
  count(nrFail) %>% 
  mutate( nrFail = 0)
z <- rbind(z,zNA) %>%
  arrange(tool_name, nrFail) %>%
  filter( nrFail<20 )

k <- z %>%
  group_by(tool_name) %>%
  mutate( c = cumsum(n), nrFindings = c/sum(n) ) %>%
  rename(Tool = tool_name)

k %>% ggplot(
  aes(x=nrFail, y=nrFindings, group=Tool)
  ) +
  geom_line( aes(linetype=Tool) ) +
  geom_point( aes(shape=Tool) ) +
  #labs(shape="Tool")+
  theme(legend.position = c(0.82, 0.32)) +
  xlab("Number of Tool Findings") +
  ylab("Cumulative Probability") +
  scale_x_continuous(trans='log10') #+ scale_y_continuous(trans='log10')

ggsave("pr_findings_vs_nrfail.pdf", plot = last_plot(), device = "pdf",
       scale = 1, height = 6, width = 9, units = "cm",
       dpi = 300)

# Most reported rules
aa <- results %>%
  group_by(rule_id, tool_name) %>%
  tally(sort=TRUE)

# Most reported rules
bb <- results %>%
  select(rule_id,tool_name,repo_id,resource) %>%
  distinct_all() %>%
  group_by(rule_id, tool_name) %>%
  tally(sort=TRUE)

