#!/bin/bash

git clone https://github.com/accurics/terrascan/
cd terrascan/pkg/policies/opa/rego/aws

for RES in $(ls)
  do
    echo $RES: [$(cat $RES/*.json | grep '"reference_id"') ], >> policies.1
  done

cat policies.1 | sed -E "s/\"reference_id\": //g" >> policies.2
cat policies.2 | sed -Ee "s/^(.*):/\"\1\":/g" > policies.3
cat policies.4 | jq > ~/thesis_iosif/tmp/resource2errID_terrascan.json

rm policies.*
