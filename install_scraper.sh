#!/bin/bash
BLUE="\e[0;94m"
NC="\e[0m"
BOLD=$(tput bold)
NORM=$(tput sgr0)

read -p "Username that will run the scripts: " USERNAME

echo -e "${BLUE}${BOLD}Installing pip3, wget, curl, time...${NC}${NORM}"
apt install python3-pip wget curl time -y
wget https://dl.google.com/go/go1.15.2.linux-amd64.tar.gz && sudo tar -C /usr/local -xzf go1.13.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.15.2.linux-amd64.tar.gz && rm go1.15.2.linux-amd64.tar.gz

echo "export GOPATH=$HOME/go" >> /home/$USERNAME/.profile
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" >> /home/$USERNAME/.profile

# Just for scraping...
# apt install chromium-driver -y

echo -e "${BLUE}${BOLD}Installing tfsec...${NC}${NORM}"
go mod init _tfsec/m
GO11MODULE=on go get github.com/aquasecurity/tfsec/cmd/tfsec@v0.48.7
rm go.mod go.sum

echo -e "${BLUE}${BOLD}Installing terrascan...${NC}${NORM}"
curl --location https://github.com/accurics/terrascan/releases/download/v1.4.0/terrascan_1.4.0_Linux_x86_64.tar.gz --output terrascan.tar.gz && tar xzf terrascan.tar.gz && ./terrascan version && mv terrascan /bin/terrascan && rm terrascan.tar.gz CHANGELOG.md LICENSE README.md
terrascan init

echo -e "${BLUE}${BOLD}Installing checkov...${NC}${NORM}"
pip3 install checkov==2.0.46 

echo -e "${BLUE}${BOLD}Installing other Python requirements...${NC}${NORM}"
pip3 install -r requirements.txt

# part for extracting & moving links
echo -e "${BLUE}${BOLD}Unpacking links...${NC}${NORM}"
tar -xf links.tar && rm links.tar
