#!/usr/bin/env python3
from   json.decoder import JSONDecodeError
import json
import glob
import os

DEBUG = True
def dbgPrint(s, breakpoint=True):
    '''
    debugging for lazy individuals, fond of
    the aquired taste for print statements
    '''
    if DEBUG:
        print(f"{s=}") # python3.8 is a godsend
        if breakpoint:
            input("Press any key to continue...")

# files produced by scrape_justLinks.py
LINKS_DIR  = f'{os.environ["HOME"]}/tf_links'
# where tool reports are gathered
SAST_DIR   = f'{os.environ["HOME"]}/tf_sast'
# where this one runs
RUNDIR     = f'{os.environ["HOME"]}/tf_sast_parsed'


if not os.path.exists(RUNDIR):
    os.mkdir(RUNDIR)

if __name__ == '__main__':

    # Figure out which resource has which rule and load it from somewhere...
    # Intended rules structure:
    #    "{resName}": ["ruleID1", "ruleID2", ...]
    # TODO (1): Brew coffee and get to manual labor
    # NOTE (1): What about non-aws rules? There are non-TF repos after all...
    
    if False:
        with open('rules_tfsec.json') as f:
            # https://tfsec.dev/docs/aws/home/
            rules_tfsec = json.load(f)
        
        with open('rules_tfsec.json') as f:
            # https://github.com/bridgecrewio/checkov/blob/master/docs/5.Policy%20Index/terraform.md
            rules_tfsec = json.load(f)
        
        with open('rules_tfsec.json') as f:
            # https://docs.accurics.com/projects/accurics-terrascan/en/latest/policies/aws.html
            rules_tfsec = json.load(f)

    # get dates for which reports are collected, and start from the present
    linkFiles = glob.glob(f'{LINKS_DIR}/*.json')
    linkFiles.sort(reverse=True)

    allReposJson = dict()

    # go one month at a time
    for linkFile in linkFiles:
        date = linkFile[-18:-11] # trimming the out [path]/DATE[_links.json]
        reportDir_1M = f'{SAST_DIR}/{date}'

        with open (linkFile, 'r') as f:
            repos_1M = json.load(f)

        # then go one repo at a time
        for repoID, repoLink in repos_1M.items():
            
            print(f"[{repoID=}]")

            # First gather files which say WHAT type of resources there are (sort by subdirectory index)
            resourceTypeFiles = glob.glob(f'{SAST_DIR}/what/{repoID}*')
            resourceTypeFiles = sorted(resourceTypeFiles, key=lambda x: int(x.rsplit('.')[0].rsplit('_')[-1]))

            # Collect which types of resources are present in the project
            ### --- [RESOURCES] --- ###
            repo_resourceTypes     = []    # per subfolder
            repo_resourceTypes_ALL = set() # per project
            for typeFile in resourceTypeFiles:
                with open(typeFile, 'r') as f:
                    repo_resourceTypes.append( set(f.read().splitlines()) )
                    repo_resourceTypes_ALL.update( repo_resourceTypes[-1] )

            
            # This should kind of catch cases where non-aws providers are skipped
            if len(repo_resourceTypes_ALL) == 0:
                continue

            
            # This will hold an intermediate list with important fields from the results
            repoDict = {'resources': list(repo_resourceTypes_ALL),
                        'tfsec'    : [],
                        'terrascan': [],
                        'checkov'  : [],
                        'files'    : []   # list of filenames, for easier debugging later
                       }


############### --- [  TFSEC  ] --- ################

# ----------- EXPECTED REPORT FORMAT ---------------
#
# {
# 	"results": [
# 		{
# 			"rule_id": "AWS093",
# 			"rule_description": "ECR Repository should use customer managed keys to allow more control",
# 			"rule_provider": "aws",
# 			"impact": "Using AWS managed keys does not allow for fine grained control",
# 			"resolution": "Use customer managed keys",
# 			"links": [
# 				"https://tfsec.dev/docs/aws/AWS093/",
# 				"https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository#encryption_configuration",
# 				"https://docs.aws.amazon.com/AmazonECR/latest/userguide/encryption-at-rest.html"
# 			],
# 			"location": {
# 				"filename": "/tmp/terraform-aws-ecr/main.tf",
# 				"start_line": 12,
# 				"end_line": 30
# 			},
# 			"description": "Resource 'aws_ecr_repository.name' does not have CMK encryption configured",
# 			"severity": "LOW",
# 			"status": "failed"
# 		}
# 	]
# }            
            tfsec_files = glob.glob(f"{reportDir_1M}/tfsec/{repoID}*.stdout")
            tfsec_files = sorted(tfsec_files, key=lambda x: int(x.rsplit('.')[0].rsplit('_')[-1]))
            
            for tfsec_file in tfsec_files:
                try:
                    with open(tfsec_file, 'r') as f:
                        report_RAW = json.load(f)
                        if 'results' in report_RAW.keys():
                            if report_RAW['results'] != None:
                                for report_item in report_RAW['results']:
                                    if report_item['rule_provider'] == 'aws': #TODO find generic
                                        _report = {'rule_id' : report_item['rule_id'],
                                                   'location': report_item['location'],
                                                   'severity': report_item['severity']}
                                        # only append if the rule/location combo is not already in the list
                                        if not any( [(r['location'] == _report['location'] and r['rule_id'] == _report['rule_id']) for r in repoDict['tfsec'] ] ):
                                            repoDict['tfsec'].append(_report)
                            else:
                                pass # literally
                                # print(f"-- {tfsec_file} (1)--")
                                # raise Exception
                        else:
                            print(f"-- {tfsec_file} (2)--")
                            raise Exception
                except JSONDecodeError:
                    pass
                except Exception as e:
                    # We want to see what causes this
                    print(f"\n[tfsec]>> {tfsec_file} \n{e=}")
                    with open(tfsec_file, 'r') as f:
                        _stdout = ''.join(f.readlines())
                        print(f"### STDOUT ###\n{_stdout=}")
                    stderrFile = os.path.splitext(tfsec_file)[0]+'.stderr'
                    with open(stderrFile, 'r') as f:
                        _stderr = ''.join(f.readlines())
                        print(f"### STDERR ###\n{_stderr=}"); input("Check it out.")
                    

############### --- [TERRASCAN] --- #############

# ----------- EXPECTED REPORT FORMAT ------------
#
# {
#   "results": {
#     "violations": [
#       {
#         "rule_name": "gkeControlPlaneNotPublic",
#         "description": "Ensure GKE Control Plane is not public.",
#         "rule_id": "accurics.gcp.NS.109",
#         "severity": "HIGH",
#         "category": "Infrastructure Security",
#         "resource_name": "gke-cluster",
#         "resource_type": "google_container_cluster",
#         "file": "gkecluster.tf",
#         "line": 1
#       }
#     ],
#     "skipped_violations": null,
#     "scan_summary": {
#       "file/folder": "/tmp/gitlab_terraform/gitlab_terraform-gke-cluster",
#       "iac_type": "terraform",
#       "scanned_at": "2021-07-27 10:40:12.043336726 +0000 UTC",
#       "policies_validated": 757,
#       "violated_policies": 1,
#       "low": 0,
#       "medium": 0,
#       "high": 1
#     }
#   }
# }
            terrascan_files = glob.glob(f"{reportDir_1M}/terrascan/{repoID}*.stdout")
            terrascan_files = sorted(terrascan_files, key=lambda x: int(x.rsplit('.')[0].rsplit('_')[-1]))
            
            for terrascan_file in terrascan_files:
                try:
                    with open(terrascan_file, 'r') as f:
                        report_RAW = json.load(f)
                        if 'results' in report_RAW.keys():
                            if 'violations' in report_RAW['results'].keys():
                                pass
                            else:
                                print(f"-- {terrascan_file} (0)--")
                                raise Exception
                            if report_RAW['results']['violations'] != None:
                                for report_item in report_RAW['results']['violations']:
                                    if report_item['rule_id'].startswith('AWS') or report_item['rule_id'].startswith('GEN'): #TODO find generic
                                        _report = {'rule_id' : report_item['rule_id'],
                                                   'location': {'file': report_item['file'], 'line': report_item['line']},
                                                   'severity': report_item['severity']}
                                        # only append if the rule/location combo is not already in the list
                                        if not any( [(r['location'] == _report['location'] and r['rule_id'] == _report['rule_id']) for r in repoDict['terrascan'] ] ):
                                            repoDict['terrascan'].append(_report)
                            else:
                                pass #literally
                                # print(f"-- {terrascan_file} (1)--")
                                # raise Exception
                        else:
                            print(f"-- {terrascan_file} (2)--")
                            raise Exception
                except JSONDecodeError:
                    pass
                except Exception as e:
                    # We want to see what causes this
                    print(f"\n[terrascan]>> {terrascan_file} \n{e=}")
                    #with open(terrascan_file, 'r') as f:
                    #    _stdout = ''.join(f.readlines())
                    #    print(f"### STDOUT ###\n{_stdout=}")
                    stderrFile = os.path.splitext(terrascan_file)[0]+'.stderr'
                    with open(stderrFile, 'r') as f:
                        _stderr = ''.join(f.readlines())
                        print(f"### STDERR ###\n{_stderr=}"); input("Check it out.")   

############### --- [ CHECKOV ] --- #############

# --------- EXPECTED REPORT FORMAT (1) ----------
#
# {
#     "check_type": "terraform",
#     "results": {
#         "failed_checks": [
#             {
#                 "check_id": "CKV_GCP_19",
#                 "check_name": "Ensure GKE basic auth is disabled",
#                 "check_result": {
#                     "result": "FAILED",
#                     "evaluated_keys": []
#                 },
#                 "code_block": [...],
#                 "file_path": "/gkecluster.tf",
#                 "repo_file_path": "/gkecluster.tf",
#                 "file_line_range": [1,6],
#                 "resource": "google_container_cluster.gke-cluster",
#                 "evaluations": null,
#                 "check_class": "checkov.terraform.checks.resource.gcp.GKEBasicAuth",
#                 "fixed_definition": null,
#                 "entity_tags": null,
#                 "caller_file_path": null,
#                 "caller_file_line_range": null,
#                 "guideline": "https://docs.bridgecrew.io/docs/bc_gcp_kubernetes_11"
#             }
#         ]
#     },
#     "summary": {
#         "passed": 4,
#         "failed": 16,
#         "skipped": 0,
#         "parsing_errors": 0,
#         "checkov_version": "2.0.46"
#     }
# }
# --------- EXPECTED REPORT FORMAT (2) ----------
# [
#   {
#       "check_type": "terraform",
#       "results": { "failed_checks": [ {..}, {..}, .. ]},
#       "summary": {..}
#   },
#   {
#       "check_type": "terraform plan",
#       "results": { "failed_checks": [ {..}, {..}, .. ]},
#       "summary": {..}
#   }
# ]

            checkov_file = f"{reportDir_1M}/checkov/{repoID}.stdout"
            if os.path.exists(checkov_file):
                try:
                    with open(checkov_file, 'r') as f:
                        report_RAW = json.load(f)
                        if type(report_RAW) == dict:
                            report_RAW = [report_RAW]
                        for subreport_RAW in report_RAW:
                            if 'results' in subreport_RAW.keys():
                                if 'failed_checks' in subreport_RAW['results'].keys():
                                    pass
                                else:
                                    print(f"-- {checkov_file} --")
                                    raise Exception
                                if len(subreport_RAW['results']['failed_checks'])>0:
                                    for report_item in subreport_RAW['results']['failed_checks']:
                                        if report_item['check_id'].startswith('CKV_AWS') or report_item['check_id'].startswith('CKV_GEN'): #TODO find generic
                                            _report = {'rule_id' : report_item['check_id'],
                                                       'location': {'file': report_item['file_path'], 'line': report_item['file_line_range'], 'resource': report_item['resource']}
                                                      }
                                            # only append if the rule/location combo is not already in the list
                                            if not any( [(r['location'] == _report['location'] and r['rule_id'] == _report['rule_id']) for r in repoDict['checkov'] ] ):
                                                repoDict['checkov'].append(_report)
                                else:
                                    pass #literally
                                    #print(f"-- {checkov_file} (1)--")
                                    #raise Exception
                            else:
                                print(f"-- {checkov_file} (2)--")
                                raise Exception
                except JSONDecodeError:
                    pass
                except Exception as e:
                    # We want to see what causes this
                    print(f"\n[checkov]>> {checkov_file} \n{e=}")
                    #with open(checkov_file, 'r') as f:
                        #_stdout = ''.join(f.readlines())
                        #print(f"### STDOUT ###\n cat ")
                    stderrFile = os.path.splitext(checkov_file)[0]+'.stderr'
                    with open(stderrFile, 'r') as f:
                        _stderr = ''.join(f.readlines())
                        print(f"### STDERR ###\n{_stderr=}"); input("Check it out.")

            repoDict['files'] = tfsec_files + terrascan_files + [checkov_file]

            os.makedirs(f"{RUNDIR}/{date}", exist_ok=True)
            with open(f'{RUNDIR}/{date}/{repoID}.json', 'w+') as f:
                json.dump(repoDict, f)

            allReposJson[repoID] = {'date'     : date,
                                    'resources': repoDict['resources'],
                                    'tfsec'    : repoDict['tfsec'],
                                    'terrascan': repoDict['terrascan'],
                                    'checkov'  : repoDict['checkov'],
                                   }

    with open(f'{RUNDIR}/allRepos.json', 'w+') as f:
        json.dump(allReposJson, f)
            


    # TODO: figure out where the duplicates are _still_ coming from (kiiiinda done?)
    # TODO: Per-subdir functionality?

    # TODO: Add this in the repo readme till the data stabilizes :
    #   https://drive.google.com/drive/folders/13YO8UIVqG0BUP9zRzIg0n17-v2VGPHfc?usp=sharing
