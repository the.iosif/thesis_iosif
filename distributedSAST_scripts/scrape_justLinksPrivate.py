#!/usr/bin/env python3
from   selenium.webdriver.support    import expected_conditions as EC
from   selenium.webdriver.support.ui import WebDriverWait
from   selenium.webdriver.common.by  import By
from   selenium                      import webdriver
from   bs4                           import BeautifulSoup
import datetime
import argparse
import requests
import shutil
import json
import time
import sys
import os


#        ..---..
#      .'  _    `.
#  __..'  (o)    :
# `..__          ;
#      `.       /
#        ;      `..---...___
#      .'                   `~-. .-')
#     .                         ' _.'
#    :     (c) Iosif Andrei      :
#    \    andrei.iosif@tum.de   '
#     +           2021         J
#      `._                   _.'
#         `~--....___...---~' mh
# 

# This script collects links of all repositories containing Terraform code (within Github's limitations).
# It does so by using Selenium alongside the chromium-chromedriver to scrape pages for information.
# The REST API approach was discarded due to rate limiting constraints:
# docs.github.com/en/rest/reference/search#rate-limit --> 10 requests per minute

#############
# CONSTANTS #
#############

DEBUG      = True
LOGFILE    = 'LOGFILE'
API        = False             # Toggle between API and Selenium scraping
FORCE      = False             # Toggle to abuse sorting options (results++, speed--)
CLEAN      = False

# Selenium requires a chrome driver, ensure it's there or else throw a tantrum
DRIVER     = '/usr/bin/chromedriver'
if not(os.path.exists(DRIVER)):
    print(f"[FATAL]: No chromedriver driver found! Expected path is:\n{DRIVER}")
    sys.exit(-1)

##################################
#    PATHS USED IN THE SCRIPT    #
##################################
# where it runs by default
RUNDIR          = os.environ['HOME']+'/tf_links.PRIVATE'

#################
#  CLI OPTIONS  #
#################

parser = argparse.ArgumentParser(description = 'Github webscraping utility (currently with plenty of hardcoding)')
parser.add_argument("--folder",     default  = RUNDIR,                    help = "folder to save files to (specify as absolute path)")
parser.add_argument("--quiet",      action   = "store_true",              help = "suppress stdout debug info (logfile will still be created)")
parser.add_argument("--clean",      action   = "store_true",              help = "nuke all created files for a fresh run")
parser.add_argument("--force",      action   = "store_true",              help = "Force the API to try and fetch more than 1000 results (8x slower)")
parser.add_argument("--api",        action   = "store_true",              help = "Use API instead of Selenium")
args = parser.parse_args()
if args.quiet:
    DEBUG  = False
if args.folder:
    RUNDIR = args.folder
if args.clean:
    CLEAN  = True
if args.force:
    FORCE    = args.force
if args.api:
    API          = True

def dbgPrint(s):
    if DEBUG:
        print(s)
        with open(os.path.join(RUNDIR,f'{LOGFILE}.log'),'a+') as f:
            f.writelines(s+'\n')

if not API:
    # The base search URL used for scraping, if using Selenium
    SEARCH_URL = 'https://github.com/search?l=&p={page}&q=updated%3A{date_updated_interval}+language%3AHCL&type=Repositories'
else:
    # if using the API, the URL looks a bit different, and we need headers in the GET
    HEADERS    = {'Accept': 'application/vnd.github.v3+json'}
    SEARCH_URL = 'https://api.github.com/search/repositories?q=language:HCL+pushed:{start}..{end}&per_page=100&page={page}'
    DATA_OF_INTEREST = ['id', 'full_name', 'created_at', 'updated_at', 'stargazers_count', 'fork', 'forks_count']


# Base search URL can be 'augmented' to get more results,
# by scraping over the results from all available sort options
if FORCE:
    QUERIES = []
    if API:
        for sort in ['author-date', 'updated', 'created', 'interactions']:
            for order in ['asc', 'desc']:
                QUERIES.append(SEARCH_URL + f'+{sort}-{order}')
    else:
        QUERIES = [SEARCH_URL] # 'best match'
        for sort in ['stars', 'updated', 'created']:
            for order in ['asc', 'desc']:
                QUERIES.append(SEARCH_URL + f'&s={sort}&o={order}')
else:
    QUERIES = [SEARCH_URL]
    

#######################
#  SELENIUM WRAPPERS  #
#######################
def reqPageSource(URL, DRIVER=DRIVER):
    """
    returns HTML page source from URL
    using DRIVER for Selenium
    """
    options = webdriver.ChromeOptions() 
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
    +"AppleWebKit/537.36 (KHTML, like Gecko)"
    +"Chrome/87.0.4280.141 Safari/537.36")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    driver = webdriver.Chrome(options=options, executable_path=DRIVER)
    driver.get(URL)
    htmlSrc = driver.page_source
    driver.quit()
    return htmlSrc

if __name__ == '__main__':

    #########
    # NOTE: # Dirs will be automatically created if they don't exist.
    #########
    if CLEAN:
        if os.path.exists(RUNDIR):
            shutil.rmtree(RUNDIR)
    
    for directory in [RUNDIR, ]:
        if not os.path.exists(directory):
            os.mkdir(directory)

    os.chdir(RUNDIR)

    # Overly-cautious debugging:
    # {LOGFILE}.{TIMESTAMP}.log backs up the log of the previous run
    # the nice thing is that {LOGFILE}.log will now contain just the log the current run.
    if os.path.exists(os.path.join(RUNDIR,f'{LOGFILE}.log')):
            os.replace(os.path.join(RUNDIR,f'{LOGFILE}.log'),\
                       os.path.join(RUNDIR,LOGFILE) +\
                       f'{round(datetime.datetime.now().timestamp())}.log')

    # We want to scrape between 01.01.2010 and the present day
    presentDay = datetime.datetime.today()
    endYear    = presentDay.year
    endMonth   = presentDay.month
    startYear  = 2010
    startMonth = 1
    dates = [datetime.date(m//12, m%12+1, 1) for m in range(startYear*12+startMonth-1, endYear*12+endMonth)]
    dates = [d.strftime('%Y-%m-%d') for d in dates]
    # fix last date for today's day (it could be longer than 1 month but oh well)
    dates[-1] = presentDay.strftime('%Y-%m-%d')
    
###########################
# Selenium-based approach #
###########################
    if not API:
        
        # All months now have 31 days :D
        dates = [f'{d}..{d[:-2]}31' for d in dates]
        # Start from the present by reversing list
        dates = dates[::-1]

        # We'll store in memory the "[user]/[repo]" part of the repo link
        linksList  = []

        # Note on github 'limitations'
        #  * 100 pages per query.
        #  * each page has 10 results.
        for d in dates:
            for p in range(1,101):
                _page_linksList = []
                dbgPrint(f"[STATUS] Scraping page {p}...")
                urlToScrape = SEARCH_URL.format(page=p, date_updated_interval=d)
                pageSrc     = reqPageSource(urlToScrape, DRIVER)
                soup        = BeautifulSoup(pageSrc, 'html.parser')

                # find those 10 results and extract repo link
                for li in soup.find_all('li', class_='repo-list-item'):
                    a = li.find('a') 
                    _page_linksList.append(a['href'][1:])
                    linksList.append(a['href'][1:])
        
                # The script stops the pages loop when it encounters no results
                # i.e. if there's only 7 pages, exit loop at the 8th iteration
                if len(_page_linksList) == 0:
                    dbgPrint(f"[STATUS] >>> Empty page. Going to next query.")
                    break
                else:
                    dbgPrint(f"[STATUS] Done scraping page {p}...")

                    # There's one file per month.
                    with open(f'links_{d[:7]}.txt','a+') as f:
                        f.writelines(link+'\n' for link in _page_linksList)
            
                    dbgPrint(f"\t[STATUS]: Done scraping page. Collected {len(_page_linksList)} repository links.")
            
            dbgPrint(f"\n\n{'-'*80}\n[STATUS] {d} Done.\n\n")

        dbgPrint(f"\n\n{'-'*80}\n[DONE] Done scraping. Collected {len(linksList)} repos.\n")

#####################
# REST API approach #
#####################
    if API:
        for i in range(1,len(dates)):
            # We start from the present and go back
            _startDate = dates[-i-1]
            _endDate   = dates[-i]

            dbgPrint(f'[STATUS]: Scraping repos updated in {_startDate[:-3]} ...\n')

            # Chunking results up into one month increments
            # (for a dataframe entry of 184B and 7000 results per month, it would total 176GB - or segfault waaay before that)
            # (df.memory_usage.sum() interrogated on one row for the 184B estimate)
            linksDict_1M = {}

            for _query in QUERIES:
                added = 0
                for p in range(1,11):
                    query = _query.format(start=_startDate, end=_endDate, page=p)
                    dbgPrint(f'[STATUS]: Q={query} ...')

                    _resp     = requests.get(query, headers=HEADERS)
                    _respJSON = _resp.json()

                    while (not _resp.ok) or ('message' in _respJSON.keys()):
                        _resp     = requests.get(query, headers=HEADERS)
                        _respJSON = _resp.json()
                        dbgPrint("[API-RESP]: Rate limit exceeded, or respCode =/= 200. Retrying after 1.5 minutes...\n.")
                        time.sleep(90)
                    
                    if _respJSON['total_count'] == 0:
                        # skip iteration for a query with no results
                        continue
                    
                    else:
                        respItems  = _respJSON['items']
                        for repo in respItems:
                            if repo['id'] not in linksDict_1M and repo['private']: # only add items not already present in gathered data TODO: add check if private
                                added += 1
                                linksDict_1M[repo['id']] = repo['full_name']
                
                dbgPrint(f'[STATUS]:\t Query done. Added {added} repos.\n\n')
            
            # end of (query in QUERIES)
            dbgPrint(f'[STATUS]: --- {_startDate[:-3]} done. Added {len(linksDict_1M)} repos. ---\n' + '-'*80 + '\n')
            
            # Save results for every month into three files:
            #
            #   [date]_allStats.csv having all the good stuff from DATA_OF_INTEREST
            #   [date]_links.csv    having an ID-to-link mapping (1 Month)
            #   link2ID.csv         having an ID-to-link  (all links)
            #   linksList.txt       having just the links (all links)

            with open('PRIVATE_id2link.csv', 'a+') as f:
                f.writelines(f'{k},{linksDict_1M[k]}\n' for k in linksDict_1M.keys())

            with open('PRIVATE_linksList.txt','a+') as f:
                f.writelines(link+'\n' for link in list(linksDict_1M.values()))
            
            # The damned rate limit
            time.sleep(60)