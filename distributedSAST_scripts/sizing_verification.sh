#!/bin/bash

# --- Machine 0 - 16 | [Grand total]: 10690

./run_sast.py --offset 0 --batch_size 2  --demo >> whichFiles.txt
./run_sast.py --offset 2 --batch_size 2  --demo >> whichFiles.txt
./run_sast.py --offset 4 --batch_size 2  --demo >> whichFiles.txt
./run_sast.py --offset 6 --batch_size 3  --demo >> whichFiles.txt
./run_sast.py --offset 9 --batch_size 2  --demo >> whichFiles.txt
./run_sast.py --offset 11 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 13 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 15 --batch_size 3 --demo >> whichFiles.txt
./run_sast.py --offset 18 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 20 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 22 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 24 --batch_size 3 --demo >> whichFiles.txt
./run_sast.py --offset 27 --batch_size 3 --demo >> whichFiles.txt
./run_sast.py --offset 30 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 32 --batch_size 2 --demo >> whichFiles.txt
./run_sast.py --offset 34 --batch_size 2 --demo >> whichFiles.txt


# --- Machine 1 - 12 | [Grand total]: 11434

./run_sast.py --offset 36 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 37 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 38 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 39 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 40 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 41 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 42 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 43 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 44 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 45 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 46 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 47 --batch_size 1 --demo >> whichFiles.txt


# --- Machine 2 - 12 | [Grand total]: 11403

./run_sast.py --offset 48 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 49 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 50 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 51 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 52 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 53 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 54 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 55 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 56 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 57 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 58 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 59 --batch_size 1 --demo >> whichFiles.txt


# --- Machine 3 - 12 | [Grand total]: 11430

./run_sast.py --offset 60 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 61 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 62 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 63 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 64 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 65 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 66 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 67 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 68 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 69 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 70 --batch_size 1 --demo >> whichFiles.txt
./run_sast.py --offset 71 --batch_size 1 --demo >> whichFiles.txt


# This is the number of files containing at least one repository link:
# $(ls ~/tf_links/*.json | xargs grep -v -rwl {} | xargs wc -l | head -n -1 | tr -s ' ' | cut -d ' ' -f 2 | wc -l)
# 
# $(cat whichFiles.txt| uniq | wc -l) contains whatever the script above did.
#
# If the two are equal, then the sizing script worked (i.e. there's no overlapping between batches)
if [[ $(ls ~/tf_links/*.json | xargs grep -v -rwl {} | xargs wc -l | head -n -1 | tr -s ' ' | cut -d ' ' -f 2 | wc -l) -eq $(cat whichFiles.txt | wc -l) ]]; then echo "ALL GOOD"; else echo "OOPS."; fi