#!/usr/bin/env python3
from   pprint import pprint
import numpy  as     np

def partition_list(a, k):
    """
    Courtesy of:
    https://gist.github.com/laowantong/ee675108eee64640e5f94f00d8edbcb4
    """
    if k <= 1: return [a]
    if k >= len(a): return [[x] for x in a]
    partition_between = [(i+1)*len(a) // k for i in range(k-1)]
    average_height = float(sum(a))/k
    best_score = None
    best_partitions = None
    count = 0

    while True:
        starts = [0] + partition_between
        ends = partition_between + [len(a)]
        partitions = [a[starts[i]:ends[i]] for i in range(k)]
        heights = list(map(sum, partitions))

        abs_height_diffs = list(map(lambda x: abs(average_height - x), heights))
        worst_partition_index = abs_height_diffs.index(max(abs_height_diffs))
        worst_height_diff = average_height - heights[worst_partition_index]

        if best_score is None or abs(worst_height_diff) < best_score:
            best_score = abs(worst_height_diff)
            best_partitions = partitions
            no_improvements_count = 0
        else:
            no_improvements_count += 1

        if worst_height_diff == 0 or no_improvements_count > 5 or count > 100:
            return best_partitions
        count += 1

        move = -1 if worst_height_diff < 0 else 1
        bound_to_move = 0 if worst_partition_index == 0\
                        else k-2 if worst_partition_index == k-1\
                        else worst_partition_index-1 if (worst_height_diff < 0) ^ (heights[worst_partition_index-1] > heights[worst_partition_index+1])\
                        else worst_partition_index
        direction = -1 if bound_to_move < worst_partition_index else 1
        partition_between[bound_to_move] += move * direction

def print_best_partition(a, k):
    print('Partitioning {0} into {1} partitions\n'.format(a, k))
    p = partition_list(a, k)
    print('The best partitioning is {0}\n    With heights {1}\n\n'.format(p, list(map(sum, p))))


###################################

# Number of repos in each month:
# ls *.json | xargs grep -v -rwl {} | xargs wc -l | head -n -1 | tr -s ' ' | cut -d ' ' -f 2

nPerRepo  = [6, 38, 65, 54, 72, 52, 76, 80, 106, 83, 124, 136, 152, 161, 145, 176, 170, 180, 262, 266, 327, 343, 355, 374, 374, 412, 417, 571, 501, 480, 547, 640, 699, 689, 780, 777, 829, 948, 976, 941, 966, 978, 966, 968, 965, 966, 972, 959, 932, 943, 949, 995, 1001, 948, 919, 1000, 927, 970, 917, 902, 891, 909, 954, 936, 1000, 905, 977, 913, 986, 995, 965, 999]
totalRepos = sum(nPerRepo)

print(f"{'-'*80}\n[2 machines x 32 cores]\n{'-'*80}")
perMachine = partition_list(nPerRepo, 2)
distributed = []
for m in perMachine:
    distributed.append(partition_list(m, 32))

offset = 0
for ix, machine in enumerate(distributed):
    print(f"--- Machine {ix} - {len(machine)} tasks containing this many repos per core / task:\n total | each month")
    for i, tasks in enumerate(machine):
        offset_next = len(tasks)
        print(f"[{str(sum(tasks)).rjust(4)}]   {tasks}" + f" --offset {offset} {' '*(2-len(str(offset)))}--batch_size {len(tasks)}".rjust(40-len(str(tasks)))) 
        offset += offset_next
    print(f"[Grand total]: {sum([sum(t) for t in machine])}\n")

print(f"#{'-'*79}\n# [2 machines x 32 cores] COMMANDS\n#{'-'*79}")

offset = 0
for ix, machine in enumerate(distributed):
    print(f"\n\n# --- Machine {ix} - {len(machine)} | [Grand total]: {sum([sum(t) for t in machine])}\n")
    for i, tasks in enumerate(machine):
        offset_next = len(tasks)
        print(f"./run_sast.py --offset {offset} --batch_size {len(tasks)} &") 
        offset += offset_next

# ---------------------------------------------------
# ---------------------------------------------------
# ---------------------------------------------------
print(f"\n\n\n{'-'*80}\n{'-'*80}\n{'-'*80}\n[4 machines x 16 cores]\n{'-'*80}")
perMachine = partition_list(nPerRepo, 4)
distributed = []
for m in perMachine:
    distributed.append(partition_list(m, 16))

offset = 0
for ix, machine in enumerate(distributed):
    print(f"--- Machine {ix} - {len(machine)} tasks containing this many repos per core / task:\n total | each month")
    for i, tasks in enumerate(machine):
        offset_next = len(tasks)
        print(f"[{str(sum(tasks)).rjust(4)}]   {tasks}" + f" --offset {offset} {' '*(2-len(str(offset)))}--batch_size {len(tasks)}".rjust(50-len(str(tasks)))) 
        offset += offset_next
    print(f"[Grand total]: {sum([sum(t) for t in machine])}\n")

print(f"#{'-'*79}\n# [4 machines x 16 cores] COMMANDS\n#{'-'*79}")

offset = 0
for ix, machine in enumerate(distributed):
    print(f"\n\n# --- Machine {ix} - {len(machine)} | [Grand total]: {sum([sum(t) for t in machine])}\n")
    for i, tasks in enumerate(machine):
        offset_next = len(tasks)
        print(f"./run_sast.py --offset {offset} --batch_size {len(tasks)} &") 
        offset += offset_next