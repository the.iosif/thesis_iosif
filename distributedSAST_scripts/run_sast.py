#!/usr/bin/env python3
import atexit
import pandas          as pd
import multiprocessing
import subprocess
import datetime
import argparse
import requests
import shutil
import json
import time
import glob
import sys
import os


#        ..---..
#      .'  _    `.
#  __..'  (o)    :
# `..__          ;
#      `.       /
#        ;      `..---...___
#      .'                   `~-. .-')
#     .                         ' _.'
#    :     (c) Iosif Andrei      :
#    \    andrei.iosif@tum.de   '
#     +           2021         J
#      `._                   _.'
#         `~--....___...---~' mh
# 


################
# DEFAULT OPTS #
################

DEBUG        = True
LOGFILE      = 'STDOUTERR'                       # .log extension gets added automatically
CLEAN        = False                             # True deletes RUNDIR and start from scratch
CLONETIMEOUT = 1*60                              # max time allowed for a git clone to finish (in seconds)
SCA_TOOLS    = ['tfsec', 'checkov', 'terrascan'] # which static code analyisis tools are used

# These parameters control the start position in the links list, as well as the sizing of the tasks.
BATCH_SIZE   = 1                                 # How many one-month chunks one task should deal with
OFFSET       = 0                                 # At which month to start
DEMO         = False                             # [DEBUG] used for sizing verification
TIMEOUT      = 30

BLACKLIST    = True                              # Skip already processed repos.

##################################
#    PATHS USED IN THE SCRIPT    #
##################################
# files produced by scrape_justLinks.py
LINKSDIR  = f'{os.environ["HOME"]}/tf_links'
# where it runs
RUNDIR    = f'{os.environ["HOME"]}/tf_sast'


#################
#  CLI OPTIONS  #
#################
parser = argparse.ArgumentParser(description   = 'SAST runner (requires a link list to be collected in advance)')
parser.add_argument("--folder",       default  = RUNDIR,                    help = "folder to save files to (specify as absolute path)")
parser.add_argument("--quiet",        action   = "store_true",              help = "suppress stdout debug info (logfile will still be created)")
parser.add_argument("--clean",        action   = "store_true",              help = "nuke all created files for a fresh run")
parser.add_argument("--clonetimeout", default  = CLONETIMEOUT,              help = "max allowed duration for 'git clone' (in seconds)")
parser.add_argument("--tools",        default  = "tfsec,terrascan,checkov", help = "csv list of tools to use. Available (default=all): tfsec, terrascan, checkov")
parser.add_argument("--batch_size",   default  = BATCH_SIZE,                help = "How many one-month chunks one task should deal with")
parser.add_argument("--offset",       default  = OFFSET,                    help = "At which month to start")
parser.add_argument("--demo",         action   = "store_true",              help = "[DEBUG] Just print which files the script will run on.")

args = parser.parse_args()
if args.folder:
    RUNDIR = args.folder
if args.quiet:
    DEBUG  = False
if args.clean:
    CLEAN  = True
if args.clonetimeout:
    CLONETIMEOUT = float(args.clonetimeout)
if args.tools:
    SCA_TOOLS    = args.tools.split(',')
if args.batch_size:
    BATCH_SIZE   = int(args.batch_size)
if args.offset:
    OFFSET       = int(args.offset)
    LOGFILE      = LOGFILE + f"_{str(args.offset)}"
if args.demo:
    DEMO         = True


# Where to save which repos time out
TIMEOUT_FILE = f'{RUNDIR}/{OFFSET}_{BATCH_SIZE}.timeout'


def dbgPrint(s):
    if DEBUG:
        print(s)
        with open(os.path.join(RUNDIR,f'{LOGFILE}.log'),'a+') as f:
            f.writelines(s+'\n')

def executeOS(cmd, path=RUNDIR, toStdout=True, toFile=False, toStr=False, quiet=False):
    # adapted from sifu's utils.py 
    # (c) 2019, 2020, Tiago Gasiba
    # pasted here to keep the script standalone.
    '''
    path :   path where the command is located (default = script location)
    cmd  :   command to execute
    ---
    returns:
      the standard output + standard error of the
      executed command separately
    '''
    runCmd = 'cd '+ path +'; ' + cmd
    if not quiet:
        print(f"[RUN] {runCmd}")
    x = subprocess.run( [runCmd],
                        shell=True,
                        capture_output=True,
                        text=True)

    if toStdout:
        print("##############")
        print("=== STDOUT ===")
        print(x.stdout)
        print("=== STDERR ===")
        print(x.stderr)
        print("##############")
    if toFile:
        with open(os.path.join(RUNDIR,f'{LOGFILE}.log'),'a+') as f:
            f.writelines("##############"+'\n')
            f.writelines(f"[RUN]{path}$\n{runCmd}"+'\n')
            f.writelines("=== STDOUT ==="+'\n')
            f.writelines(x.stdout+'\n')
            f.writelines("=== STDERR ==="+'\n')
            f.writelines(x.stderr+'\n')
            f.writelines("##############"+'\n')

    if toStr:
        strForm  = ''
        strForm += "##############"+'\n'
        strForm += f"[RUN]{path}$\n{runCmd}"+'\n'
        strForm += "=== STDOUT ==="+'\n'
        strForm += x.stdout+'\n'
        strForm += "=== STDERR ==="+'\n'
        strForm += x.stderr+'\n'
        strForm += "##############"+'\n'
        retObj = {'stdout': x.stdout, 'stderr':x.stderr, 'retcode': x.returncode, 'str': strForm}
    else:
        retObj = {'stdout': x.stdout, 'stderr':x.stderr, 'retcode': x.returncode}            
    return retObj


################################################################################
def cleanup(repoAbsPath):
    '''
    removes leftover cloned repo
    if the script exits unexpectedly
    '''
    if os.path.exists(repoAbsPath):
        print(f"\n\n[CLEANUP] Removing {repoAbsPath}...")
        shutil.rmtree(repoAbsPath)
        print(f"[CLEANUP] Done. Exiting.")
################################################################################

if __name__ == '__main__':

    _NOW = round(datetime.datetime.now().timestamp())
  
    #########
    # NOTE: # Dirs will be automatically created if they don't exist.
    #########

    if CLEAN:
        if os.path.exists(f"{RUNDIR}"):
            shutil.rmtree(f"{RUNDIR}")

    for directory in [RUNDIR, f"{RUNDIR}/time", f"{RUNDIR}/what"]:
        if not os.path.exists(directory):
            os.mkdir(directory)

    # now we can proceed
    os.chdir(RUNDIR)

    # Overly-cautious debugging:
    # {LOGFILE}.{TIMESTAMP}.log backs up the log of the previous run
    # the nice thing is that {LOGFILE} will now contain just the log the current run.
    if os.path.exists(os.path.join(RUNDIR,f'{LOGFILE}.log')):
            os.replace(os.path.join(RUNDIR,f'{LOGFILE}.log'),\
                       os.path.join(RUNDIR,LOGFILE) +\
                       f'{_NOW}.log')

    # Check over the link json's which are the non-empty ones
    # and use that as the list to iterate over.
    json_files = executeOS('ls *.json | xargs grep -v -rwl {}', path=LINKSDIR, toStdout=False, toFile=False, toStr=False, quiet=True)
    json_files = json_files['stdout'].split('\n')[:-1]
    json_files = json_files[::-1] # revert list to start from the present


    if DEMO:
        print(f"---- Offset {OFFSET} Batch-Size {BATCH_SIZE}")

    doneBatches = 0 #increments when a batch downloaded the max # of repos
    added       = [-99]

    # Exit when no more repos are added OR in the unlikely case that all batches are done
    while doneBatches < BATCH_SIZE or added[-1]!=0:
        
        # Last item of added will contain number of processed repos for this iteration
        added.append(0)

        for _jsonFile in json_files[OFFSET:OFFSET+BATCH_SIZE]:

            if DEMO:
                # just print filename
                with open(f"{LINKSDIR}/{_jsonFile}", 'r') as f:
                    try:
                        repos_1M = json.load(f)
                    except Exception as e:
                        dbgPrint(f"[FATAL] Can't load links json: {e}")
                print(f"{_jsonFile} {len(repos_1M)}")
                continue
            
            # get YYYY-MM representation of date from the json filename
            date = _jsonFile[:7] 

            # For the sake of it, check if links file exists before doing anything
            if not os.path.exists(f"{LINKSDIR}/{_jsonFile}"):
                dbgPrint(f"[FATAL] File not found: {LINKSDIR}/{_jsonFile}")
                continue
            else:
                with open(f"{LINKSDIR}/{_jsonFile}", 'r') as f:
                    try:
                        repos_1M = json.load(f)
                        if len(repos_1M) == 0:
                            dbgPrint(f"[INFO] No repos colected in {date}.")
                            continue
                    except Exception as e:
                        dbgPrint(f"[FATAL] Can't load links json: {e}")
                        sys.exit(-1)

            # Assemble blacklist based on previously collected reports
            blacklist = glob.glob(f"{RUNDIR}/{date}/checkov/*.stdout")                     # get abspaths for existing checkov reports
            blacklist = set([os.path.splitext(os.path.basename(f))[0] for f in blacklist]) # trim down to repo ID

            # Add to the blacklist the set of repos which don't have any TF files whatsoever
            if os.path.exists(f"{RUNDIR}/noTF_{date}.blacklist"):
                with open (f"{RUNDIR}/noTF_{date}.blacklist", "r") as f:
                    _noTFrepos = f.read().splitlines()
                    for _repoID in _noTFrepos:
                        blacklist.add(_repoID)
            
            # Add to the blacklist the set of repos which marked as private from previous runs
            if os.path.exists(f"{RUNDIR}/{TIMEOUT_FILE}"):
                with open (f"{RUNDIR}/{TIMEOUT_FILE}", "r") as f:
                    _privateRepos = f.read().splitlines()
                    for _repoID in _privateRepos:
                        blacklist.add(_repoID)

            # Create directory structure for saving reports:
            # RUNDIR/reports/[date]/
            reportSavePath_1M = f'{RUNDIR}/{date}'
            if not os.path.exists(reportSavePath_1M):
                    os.mkdir(reportSavePath_1M)
            # RUNDIR/reports/[date]/[tool]
            for tool in SCA_TOOLS:
                tool_report_dir = f'{reportSavePath_1M}/{tool}'
                if not os.path.exists(tool_report_dir):
                    os.mkdir(tool_report_dir)

            dbgPrint(f'[STATUS]: Running SAST on repos updated in {date} ...\n')

            # Last item of added will contain number of processed repos for this iteration
            added.append(0)

            for repoID, repoLink in repos_1M.items():
                # type(RepoID) = string

                isDone = (len(repos_1M) - len(blacklist)) == 0
                if isDone:
                    dbgPrint(f"[BATCH DONE!!!] Done for {date}")
                    doneBatches += 1
                    break

                dbgPrint(f"[BL {date}] #items ({len(repos_1M)}) - #blacklist({len(blacklist)}) = {len(repos_1M) - len(blacklist)}")

                if repoID in blacklist:
                    continue

                dbgPrint(f"\n\n\n{'-'*80}\n[REPO] {repoLink}\n{'-'*80}")
                # Repo is cloned first. Before adding the repo to the dataset and runnning SCA tools,
                # The following two checks must be passed:
                #   1. cloning must not exceed CLONETIMEOUT
                #   2. there must be at least one folder in the repo with .tf files
                repoName    = repoLink.split('/')[-1]
                repoAbsPath = os.path.join('/tmp', repoName)

                # [CHECK 1] -- attempt cloning
                if not CLONETIMEOUT:  # clone without timeout
                    executeOS(f"git clone --depth 1 https://github.com/{repoLink}", path='/tmp')
                else:
                    #git clone from different process, so we can stop if it takes too long
                    
                    # (this would be the case for gigabyte-sized repos, there's some...)
                    # Later finding: this is also the case for now-private repos...
                    cloneCmd = f"git clone --depth 1 https://github.com/{repoLink}"
                    cloneProcess = multiprocessing.Process(target=executeOS,
                                                        kwargs={'cmd':cloneCmd, 'path':'/tmp'})
                    cloneProcess.start() 
                    cloneProcess.join(CLONETIMEOUT)
                    if cloneProcess.is_alive(): 
                        cloneProcess.terminate()
                        cloneProcess.join()
                        
                        # (if it took too long: rmtree and pass over this loop)
                        if os.path.exists(repoAbsPath):
                            shutil.rmtree(repoAbsPath)                        
                        
                        # We want to see what's going on here
                        runCmd = f"echo {repoID} >> {TIMEOUT_FILE}"
                        subprocess.run( [runCmd], shell=True )
                        
                        continue
                
                # [CHECK 1] -- cloning happened within the timeout if we reached this point
                #
                # if the process exits unexpectedly
                # we need to provide a clean state
                # for the next run (i.e. no leftover folders)
                atexit.register(cleanup, repoAbsPath)

                # [CHECK 2] -- checking that there is .tf files somewhere in the repo
                # get all subdirs inside the repo (includes root repo dir)
                repoDirList = [x[0] for x in os.walk(repoAbsPath)]
                # extracting dirs that contain .tf files
                repoTfDirList = [d for d in repoDirList  if any([f.endswith('.tf') for f in os.listdir(d)])]
                
                # it may happen that we clone repos without any .tf files at all
                if len(repoTfDirList) == 0:
                    # no race condition, since batches don't overlap on the same date
                    runCmd = f"echo {repoID} >> {RUNDIR}/noTF_{date}.blacklist"
                    print(f"[RUN, BL]\n {runCmd}")
                    subprocess.run( [runCmd], shell=True )
                    if os.path.exists(repoAbsPath):
                        shutil.rmtree(repoAbsPath)
                    atexit.unregister(cleanup)
                    continue

            ###### SCA will run one tool at a time over the repo list
                        
            ###### --- tfsec --- ###
                # walk all subdirs in the repo
                # and keep index for subfolders with *.tf files
                # NOTE: not using --force-all-dirs because tfsec can't pick up the tfvars file on its own...
                idx              = 1
                for repoDir in repoTfDirList:
                    
                    # Check which resource types exist in the subfolder
                    cmd = "cat *.tf *.tfvars 2>/dev/null | grep '^ *variable \+\"'  | awk '{print $1 \" \" }'   | sort -u >  "+ f"{RUNDIR}/what/{repoID}_{idx}.what;\\\n" +\
                          "cat *.tf                      | grep '^ *resource \+\"'  | awk '{print $1 \" \" $2}' | sort -u >> "+ f"{RUNDIR}/what/{repoID}_{idx}.what;\\\n" +\
                          "cat *.tf                      | grep '^ *data \+\"'      | awk '{print $1 \" \" $2}' | sort -u >> "+ f"{RUNDIR}/what/{repoID}_{idx}.what;\\\n" +\
                          "cat *.tf                      | grep '^ *output \+\"'    | awk '{print $1 \" \" }'   | sort -u >> "+ f"{RUNDIR}/what/{repoID}_{idx}.what"
                    runCmd = 'cd '+ repoDir +'; ' + cmd
                    print(f"[RUN, WHAT]\n {runCmd}")
                    subprocess.run( [runCmd], shell=True )
                    
                    if 'tfsec' in SCA_TOOLS:
                        # passing tfvars file to tfsec
                        if 'variables.tf' in os.listdir(repoDir): # edge case... 
                            _tfvars = f'--tfvars-file variables.tf'
                        elif any([x.endswith('.tfvars') for x in os.listdir(repoDir)]): # usual case
                            _tfvars = [f for f in os.listdir(repoDir) if f.endswith('.tfvars')][0]
                            _tfvars = f'--tfvars-file {_tfvars}'
                        else:
                            _tfvars = ''
                        
                        stdoutFile = f"{reportSavePath_1M}/tfsec/{repoID}_{idx}.stdout"
                        stderrFile = f"{reportSavePath_1M}/tfsec/{repoID}_{idx}.stderr"
                        timeFile   = f"time_{repoID}_{idx}_tfsec.txt"

                        cmd    = f"/usr/bin/time -f '%E' -o {RUNDIR}/time/{timeFile} timeout {TIMEOUT} ~/go/bin/tfsec . {_tfvars} -f json > {stdoutFile} 2> {stderrFile}"
                        runCmd = 'cd '+ repoDir +'; ' + cmd
                        print(f"[RUN, TFSEC] {runCmd}")
                        subprocess.run( [runCmd], shell=True )
                                                
            ###### --- terrascan --- ###
                    if 'terrascan' in SCA_TOOLS:
                        # terrascan also doesn't seem to know the concept of nested folders,
                        # therefore it will also be added in this loop


                        # This tool hangs badly on some repos, therefore:
                        # Launching a subprocess with timeout and mark as exception if it exceeds the timeout.
                            # EXCEPTION DOCUMENTATION FOR RECREATION:
                            # git clone --depth 1 https://github.com/microsoft/bedrock &&\
                            # cd bedrock/cluster/environments/azure-multiple-clusters-waf-tm-apimgmt &&\
                            # terrascan scan -o json

                        stdoutFile = f"{reportSavePath_1M}/terrascan/{repoID}_{idx}.stdout"
                        stderrFile = f"{reportSavePath_1M}/terrascan/{repoID}_{idx}.stderr"
                        timeFile   = f"time_{repoID}_{idx}_terrascan.txt"
                        
                        cmd    = f"/usr/bin/time -f '%E' -o {RUNDIR}/time/{timeFile} timeout {TIMEOUT} terrascan scan -o json > {stdoutFile} 2>{stderrFile}"
                        runCmd = 'cd '+ repoDir +'; ' + cmd
                        print(f"[RUN, TERRASCAN] {runCmd}")
                        subprocess.run( [runCmd], shell=True )
                        
                    idx += 1

            ####### --- checkov --- ###
                # it can run on the whole repo without problems
                if 'checkov' in SCA_TOOLS:
                    
                    stdoutFile = f"{reportSavePath_1M}/checkov/{repoID}.stdout"
                    stderrFile = f"{reportSavePath_1M}/checkov/{repoID}.stderr"
                    timeFile   = f"time_{repoID}_checkov.txt"

                    cmd    = f"/usr/bin/time -f '%E' -o {RUNDIR}/time/{timeFile} timeout {TIMEOUT} checkov --compact --quiet -d . -o json > {stdoutFile} 2>{stderrFile}"
                    runCmd = 'cd '+ repoDir +'; ' + cmd
                    print(f"[RUN, CHECKOV] {runCmd}")
                    subprocess.run( [runCmd], shell=True )

                
                # once done, remove repo and objects
                if os.path.exists(repoAbsPath):
                    shutil.rmtree(repoAbsPath)
                atexit.unregister(cleanup)

                # Increment number of added items
                added[-1] += 1

            # Note to self for later processing:
            # Extracting stdout
            # stdOut = REPORT.partition('=== STDOUT ===\n')[2].partition('=== STDERR ===\n')[0])
            # StdErr = REPORT.partition('=== STDOUT ===\n')[2].partition('=== STDERR ===\n')[2].rstrip('\n##############')

        
        dbgPrint("\n\n[DONE] Iteration completed normally")

        # NOTE ON SIZING:
        # Number of repos in each month:
        # ls *.json | xargs grep -v -rwl {} | xargs wc -l | head -n -1 | tr -s ' ' | cut -d ' ' -f 2
        # 